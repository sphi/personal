#!/data/data/com.termux/files/usr/bin/bash

PACKAGE=com.pandora.android
ACTIVITY="$PACKAGE/.LauncherActivity"

echo "killing $PACKAGE..."
su -c am force-stop $PACKAGE
sleep 1

echo "starting $ACTIVITY..."
su -c am start --user 10 -n "$ACTIVITY"
