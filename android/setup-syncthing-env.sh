#!/usr/bin/env bash
set -euo pipefail

# Attermpts to detect Syncthing binary and API keys, and uses them to write ~/.config/syncthing.env

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
BLUE="\x1b[34m"
GREEN="\x1b[32m"
RED="\x1b[31m"

if test -z "${ANDROID_ROOT:-}"; then
    printf "${RED}Not on Android$EOL"
    exit 1
fi

mkdir -p "$HOME/.config"
SYNCTHING_ENV_PATH="$HOME/.config/syncthing.env"
SYNCTHING_BIN=''
SYNCTHING_API_KEY=''

function save_syncthing_env() {
    echo "# SYNCTHING_BIN can be set to 'syncthing' if the syncthing package is installed
# this seems to work, unclear how likely it is for compatibility to break if versions don't match
export SYNCTHING_BIN='$SYNCTHING_BIN'
export SYNCTHING_API_KEY='$SYNCTHING_API_KEY'
" >"$SYNCTHING_ENV_PATH"
}

if test -e "$SYNCTHING_ENV_PATH"; then
    printf "$SYNCTHING_ENV_PATH already exists, sourcing it...\n"
    source "$SYNCTHING_ENV_PATH"
else
    printf "saving initial $SYNCTHING_ENV_PATH with empty values\n"
    save_syncthing_env
fi

printf "detecting syncthing PID...\n"
SYNCTHING_PID=$(sudo lsof -i :8384 | grep LISTEN | tr -s ' ' | cut -d ' ' -f 2)

printf "syncthing PID is ${BLUE}$SYNCTHING_PID${NORMAL}, detecting syncthing binary...\n"
SYNCTHING_BIN="$(sudo readlink /proc/$SYNCTHING_PID/exe)"
# SYNCTHING_BIN is something like:
# /data/app/~~MTb3Sgujsik8HXhFtgVqXA==/com.nutomic.syncthingandroid-jMJCr3s9fMpEsdst0n-nRw==/lib/arm64/libsyncthing.so

printf "syncthing binary is ${BLUE}$SYNCTHING_BIN${NORMAL}, detecting app ID...\n"
# App ID may be different if, for example, we're running syncthing fork
# Not that this will fail if app ID has a - in it
APP_ID="$(echo "$SYNCTHING_BIN" | grep -Po '(?<===/)[^/\-]+\.[^/\-]+(?=\-.*==/)')"
# APP_ID is something like
# com.nutomic.syncthingandroid

printf "app ID is ${BLUE}$APP_ID${NORMAL}, detecting config file...\n"
CONFIG_PATH="/data/user/0/$APP_ID/files/config.xml"
# CONFIG_PATH is something like:
# /data/user/0/com.nutomic.syncthingandroid/files/config.xml

if sudo test -f "$CONFIG_PATH"; then
    printf "config file is ${BLUE}$CONFIG_PATH${NORMAL}, detecting API key...\n"
    SYNCTHING_API_KEY=$(sudo cat "$CONFIG_PATH" | grep -Po '(?<=<apikey>).*(?=<)')
    printf "API key is ${BLUE}$SYNCTHING_API_KEY${NORMAL}, saving in $SYNCTHING_ENV_PATH...\n"
else
    printf "config file not found at ${RED}$CONFIG_PATH${NORMAL}\n"
    # We might have already loaded the API key from the .env file
    if test ! -z "$SYNCTHING_API_KEY"; then
        printf "using API key ${BLUE}$SYNCTHING_API_KEY${NORMAL}, loaded from $SYNCTHING_ENV_PATH\n"
    else
        printf "${RED}you will have to manually look up API key in syncthing web UI and copy it into $SYNCTHING_ENV_PATH$EOL"
    fi
fi

save_syncthing_env
printf "${GREEN}$SYNCTHING_ENV_PATH saved$EOL"
