#!/usr/bin/env bash
set -euo pipefail
DIR="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")"
SCRIPT="$DIR/../tools/syncthing-wipe.sh"
read -p "Command: " CMD
case "$CMD" in
"" | "wipe")
    echo "Wiping Light..."
    bash "$SCRIPT" wipe Light
    ;;
"restore")
    echo "Restoring Light..."
    bash "$SCRIPT" restore /storage/emulated/0/Documents/Light.json
    ;;
"help")
    echo "Commands:"
    echo "  wipe    - wipe the Light directory"
    echo "  restore - restore the Light directory"
    ;;
*)
    echo "Unknown command"
    exit 1
    ;;
esac
