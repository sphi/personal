#!/usr/bin/env bash
set -euo pipefail

# Can be run with:
# curl https://codeberg.org/sphi/personal/raw/branch/main/android/android-setup.sh | bash

# ncurses-utils is needed for tput which is used by the weather script
PACKAGES="openssh tsu vim git python3 jq grep which ripgrep lsof ncurses-utils"
CODE="$HOME/code"

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
BLUE="\x1b[34m"
GREEN="\x1b[32m"
RED="\x1b[31m"

if test -z "${ANDROID_ROOT:-}"; then
    printf "${RED}not on Android$EOL"
    exit 1
else
    printf "${GREEN}on Android$EOL"
    echo
fi

install_symlink() {
  if test -L "$2"; then
    if test "$(realpath "$2")" == "$1"; then
      printf "${GREEN}$2 already linked to $1$EOL"
      return
    else
      printf "${BLUE}$2 linked to $(realpath "$2"), removing...$EOL"
      rm "$2"
    fi
  elif test -e "$2"; then
    printf "${RED}$2 already exists, needs to be removed$EOL"
    exit 1
  fi
  printf "${BLUE}linking $1 to $2$EOL"
  ln -sv "$1" "$2"
  echo
}

clone_git_repo() {
  if ! test -d "$CODE/$2"; then
    printf "${BLUE}cloning $2 git repo$EOL"
    (cd "$CODE" && git clone "$1" "$2")
    git config --global --add safe.directory "$(realpath "$CODE/$2")"
    echo
  else
    printf "${BLUE}pulling $2 git repo$EOL"
    (cd "$CODE/$2" && git pull)
    echo
  fi
}

printf "${BLUE}updating and installing packages$EOL"
pkg update -y
pkg upgrade -y
pkg install $PACKAGES -y
echo

if ! ls "/storage/emulated/0" >/dev/null; then
  printf "${RED}failed to access /storage/emulated/0, you may need to grant termux permissions$EOL"
  exit 1
fi

mkdir -p "/storage/emulated/0/Documents/code"
mkdir -p "$HOME/.config"
install_symlink "/storage/emulated/0/Documents/code" "$HOME/code"
install_symlink "/storage/emulated/0/" "$HOME/home"
if test ! -d "/storage/emulated/0/Documents/Light"; then
  printf "${RED}Light does not exist at expected path$EOL"
  exit 1
fi
install_symlink "/storage/emulated/0/Documents/Light/Config/ssh" "$HOME/ssh"
install_symlink "/storage/emulated/0/Documents/Light/Config/wifi-sync" "$HOME/.config/wifi-sync"
install_symlink "/data/media/10" "$HOME/work_home"

if test -f "$HOME/ssh.sh"; then
  printf "${GREEN}$HOME/ssh.sh already exists$EOL"
else
  printf "${BLUE}creating $HOME/ssh.sh$EOL"
  echo '#!/usr/bin/echo file should be sourced, not ran:
eval $(ssh-agent -s)
ssh-add "$HOME/ssh/id_rsa"
' >"$HOME/ssh.sh"
fi

printf "${GREEN}Setting git config options$EOL"
git config --global user.email "git@phie.me"
git config --global user.name "Sophie Winter"
echo

printf "${GREEN}enabling SSH$EOL"
source "$HOME/ssh.sh"
echo

clone_git_repo git@codeberg.org:sphi/personal.git personal
clone_git_repo git@github.com:wmww/wifi-sync.git wifi-sync

printf "${BLUE}creating shortcuts$EOL"
mkdir -p "$HOME/.shortcuts"
ANDROID_PATH="$HOME/code/personal/android"
printf "#!/usr/bin/bash\nexport EXIT_ON_SUCCESS=1\nbash $ANDROID_PATH/run-shortcut.sh bash $HOME/code/personal/tools/weather.sh -i\n" >"$HOME/.shortcuts/weather"
chmod +x "$HOME/.shortcuts/weather"
printf "#!/usr/bin/bash\nbash $ANDROID_PATH/run-shortcut.sh bash $ANDROID_PATH/manage-syncthing.sh\n" >"$HOME/.shortcuts/syncthing"
chmod +x "$HOME/.shortcuts/syncthing"
printf "#!/usr/bin/bash\nbash $ANDROID_PATH/run-shortcut.sh python3 $HOME/code/wifi-sync/wifi-sync update\n" >"$HOME/.shortcuts/wifi-sync"
chmod +x "$HOME/.shortcuts/wifi-sync"
printf "#!/usr/bin/bash\nbash $ANDROID_PATH/run-shortcut.sh bash $HOME/code/personal/android/android-setup.sh\n" >"$HOME/.shortcuts/setup"
chmod +x "$HOME/.shortcuts/setup"
echo

printf "${GREEN}success!$EOL"
