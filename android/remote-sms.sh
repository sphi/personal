#!/usr/bin/env bash
set -euo pipefail

topic_url="ntfy.sh/tess-topic-0"
send_to=""

function send_message {
  echo "Sending reply: $1"
  curl -d "# $1" "$topic_url"
  sleep 1 # Prevents message loops from going too fast
}

function print_help {
  echo "USAGE:"
  echo "/help: show this help message"
  echo "/to NUMBER: set the number to send to"
  echo "/send MESSAGE: send a message"
  echo "/list: list recent messages"
  echo "/quit: exit the script"
}

function list_sms {
  list="$(termux-sms-list)"
  length="$(jq length <<<"$list")"
  echo "Recent messages:"
  for ((i=0; i<$length; i++)); do
    item="$(jq ".[$i]" <<<"$list")"
    echo "$(jq -r .sender <<<"$item") ($(jq -r .number <<<"$item")): $(jq -r .body <<<"$item")"
  done
}

function handle_msg {
  echo "Got message: $1"
  if test "$(jq -r .event <<<"$1")" == message; then
    content="$(jq -r .message <<<"$1")"
    command="$(head -n 1 <<<"$content" | cut -f 1 -d ' ')"
    body="$(cut -f 2- -d ' ' <<<"$content")"
    echo "Command: $command"
    
    case "$command" in
    
    "#")
      echo "Ignoring message (from us)"
      ;;
      
    "/help")
      send_message "$(print_help)"
      ;;
      
    "/to")
      if grep -Pq '^[0-9+ \-]+$$' <<<"$body"; then
        send_to="$body"
      else
        send_message "'$body' is not a phone number you buffoon"
      fi
      ;;
      
    "/send")
      if test -z "$send_to"; then
        send_message "Error: please specify a recipient with /to"
      else
        echo "Sending: $body"
        termux-sms-send -n "$send_to" "$body"
      fi
      ;;
      
    "/list")
      send_message "$(list_sms)"
      ;;
    
    "/quit")
      send_message "Exiting"
      exit 0
      ;;

    *)
      send_message "Invalid command '$command', try /help"
      ;;
    esac
  fi
}

echo "To send messages go to $topic_url"
echo
stdbuf -i0 -o0 curl -s "$topic_url/json" | while IFS= read -r line; do
  handle_msg "$line"
done