#!/usr/bin/env bash
set -euo pipefail

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
PURPLE="\x1b[35m"
GREEN="\x1b[32m"
RED="\x1b[31m"

ALL_ARGS="$@"
printf "${PURPLE}$ %s$EOL\n" "$ALL_ARGS"
if $ALL_ARGS; then
    printf "\n${GREEN}complete$NORMAL "
    if test "${EXIT_ON_SUCCESS:-}" == 1; then
        echo
        exit
    fi
else
    printf "\n${RED}failed with exit code $?$NORMAL "
fi
printf "[press ENTER to close]$EOL"
read
