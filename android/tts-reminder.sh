#!/usr/bin/env bash
set -euo pipefail

get_min() {
  bc <<<"$(date +"%H")*60+$(date +"%M")"
}

DURATION="$1"
START=$(get_min)
END=$(bc <<<"$START+$DURATION")
while true; do
  NOW=$(get_min)
  TO_GO=$(bc <<<"$END-$NOW")
  echo "Minutes to go: $TO_GO"
  if test $(bc <<<"$TO_GO<0") == 1; then
    termux-tts-speak "Time's up!";
  fi;
  sleep 1m;
done