#!/usr/bin/env python3
from typing import Optional
import logging
import os
import subprocess

logging.basicConfig(level=logging.INFO)

def current_options() -> dict[str, str]:
    if not os.path.exists(os.path.expanduser('~/.gitconfig')):
        # git config --global --list fails in this case lol
        return {}
    result = subprocess.run(['git', 'config', '--global', '--list'], capture_output=True, text=True, check=True).stdout.strip()
    current = {}
    for line in result.splitlines():
        if line:
            key_val = line.split('=', 1)
            if len(key_val) == 2:
                current[key_val[0]] = key_val[1]
    return current

def setup_options(options: dict[str, str]) -> None:
    current = current_options()
    changed = 0
    for k, v in options.items():
        if k not in current or current[k] != v:
            subprocess.run(['git', 'config', '--global', k, v], check=True)
            changed += 1
    if changed > 0:
        logging.info('set %d git option(s)', changed)
    else:
        logging.info('all git options were already correct')

def load_options() -> dict[str, str]:
    # Load from Light/Config/git.conf.py
    path = os.path.dirname(__file__) + '/git.conf.py'
    contents = open(path, 'r').read()
    options = eval(contents)
    assert isinstance(options, dict), 'options is not a dict'
    for i, (k, v) in enumerate(options.items()):
        assert isinstance(k, str), 'key is not a string'
        assert isinstance(v, str), 'value is not a string'
    return options
    
if __name__ == '__main__':
    options = load_options()
    setup_options(options)
