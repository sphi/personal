#!/bin/sh
dbus-update-activation-environment --systemd "XDG_CURRENT_DESKTOP=sway" "WAYLAND_DISPLAY=$WAYLAND_DISPLAY"
export PULSE_SERVER="unix:/tmp/pipewire-pulse-socket" # Use the global pipewire-pulse socket
export MOZ_ENABLE_WAYLAND=1 # Make Firefox use Wayland
export XDG_SESSION_TYPE=wayland # Needed for Firefox to use xdg-desktop-portal
exec "$@"
