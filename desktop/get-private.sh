#!/usr/bin/env bash
set -euo pipefail

NORMAL="\x1b[0m"
GREEN="\x1b[32m"
RED="\x1b[31m"
ME=$(basename $0)

if test $# -ge 2; then
  printf "$ME: ${RED}too many arguments, see --help${NORMAL}\n" >&2
  exit 1
elif test $# -eq 1 && test "$1" == "--help"; then
  printf 'USAGE
  PRIV=$("$(dirname "$(readlink -f "$0")")/../config/%s")
  PRIV=$("$(dirname "$(readlink -f "$0")")/../config/%s" /sub/directory)
  
make sure your script has "set -e" and only uses this script to assign a variable, or else it will not fail if directory doesn'"'"'t exist
' "$ME" "$ME" >&2
  exit 1
fi

PRIV="$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/private"
INSTALL_CMD="ln -s $HOME/light/Config $PRIV"
if test ! -L "$PRIV"; then
  printf "${RED}$ME: $PRIV doesn't exist${NORMAL}\n" >&2
  printf "$ME: you may want to run ${GREEN}$INSTALL_CMD${NORMAL}\n" >&2
  exit 1
fi
REAL_PRIV="$(readlink -f $PRIV || true)"
if test ! -d "$REAL_PRIV"; then
  printf "${RED}$ME: $PRIV is a broken symlink${NORMAL}\n" >&2
  printf "$ME: you may want to run ${GREEN}rm $PRIV && $INSTALL_CMD${NORMAL}\n" >&2
  exit 1
fi
if test $# -ge 1; then
    REAL_PRIV="$REAL_PRIV/$1"
    if test ! -e "$REAL_PRIV"; then
      printf "${RED}$ME: $REAL_PRIV doesn't exist${NORMAL}\n" >&2
      exit 1
    fi
fi
echo "$REAL_PRIV"

