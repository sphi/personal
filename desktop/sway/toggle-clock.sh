#!/bin/bash
set -euo pipefail
if which cairo-clock; then
  CLOCK="cairo-clock"
  ARGS=""
elif which xclock; then
  CLOCK="xclock"
  ARGS="-fg #60b0d0 -hl #60b0d0 -hd #60b0d0 -bg #303030 -norender -analog -geometry 150x150"
else
  CLOCK="zenity"
  ARGS="--error --text=no_clock_found"
fi

if test ! -z "$(pidof "$CLOCK")"; then
  killall "$CLOCK"
else
  exec "$CLOCK" $ARGS
fi
