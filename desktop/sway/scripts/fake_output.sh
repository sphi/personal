#!/usr/bin/env bash
set -euo pipefail

# See https://www.reddit.com/r/swaywm/comments/k1zl41/thank_you_devs_free_ipad_repurposed_as_a_second/
# TODO: finish this script

MODE="1920x1080"
POS_X="-1920"
POS_Y="0"

# Create fake output
swaymsg create_output
# Get the name of the last output (doesn't seem to be any way to give out output a specific name)
OUTPUT_NAME="$(swaymsg -rt get_outputs | jq -r '.[-1].name')"

# Handle SIGINT
handle_interrupt() {
    echo "Removing $OUTPUT_NAME"
    swaymsg output "$OUTPUT_NAME" unplug
}
trap handle_interrupt SIGINT

# Set up the output
swaymsg output "$OUTPUT_NAME" -- mode "$MODE" pos "$POS_X" "$POS_Y" scale 1

echo "Now go to https://screensy.marijn.it/#tesspriv on both devices"
