#!/usr/bin/env bash
set -euo pipefail
DIR="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")"

arg="$1"
amixer sset Master "$arg"

"$DIR/level-display.sh" volume 'amixer sget Master | (grep -Po "(?<=\[)\d+(?=%\] \[on)"; echo 0) | head -n 1'
