chmod o=x "$XDG_RUNTIME_DIR"
chmod o=rw "$XDG_RUNTIME_DIR/$WAYLAND_DISPLAY"
xhost +SI:localuser:code
xhost +SI:localuser:play
xhost +SI:localuser:comms
