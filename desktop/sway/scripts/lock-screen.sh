#!/usr/bin/env bash
set -euo pipefail
#  --ignore-empty-password \
swaylock \
  --daemonize \
  --image "/root/Desktop/arch-space.jpg" \
  --scaling fill \
  --indicator-y-position 900 \
  --line-uses-inside \
  --line-color '00000000' \
  --layout-border-color '00000000' \
  --separator-color '00000000' \
  --line-ver-color '00000000' \
  --line-wrong-color '00000000' \
  --ring-color '181b28' \
  --key-hl-color 'c50ed2' \
