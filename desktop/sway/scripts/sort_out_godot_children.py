#!/usr/bin/python3

import subprocess
import json

def should_be_floating(node):
    # If this is a non-toplevel godot node that is not yet floating
    return (
        'window_properties' in node and
        (node['window_properties'].get('class') == 'Godot' or node['window_properties'].get('instance') == 'Godot_Engine') and
        node['window_properties'].get('transient_for') and
        node['type'] != 'floating_con')

def find_nodes(node, pred):
    if pred(node):
        return [node]
    else:
        matching = []
        for child in node['nodes']:
            matching += find_nodes(child, pred)
        for child in node['floating_nodes']:
            matching += find_nodes(child, pred)
        return matching

def swaymsg(options, args):
    args = ['swaymsg'] + options + ['--'] + args
    result = subprocess.run(args, capture_output=True, text=True)
    if result.returncode != 0:
        raise RuntimeError('`' + ' '.join(args) + '` failed:\n' + result.stderr)
    return result.stdout

if __name__ == '__main__':
    result = swaymsg(['-t', 'get_tree'], [])
    tree = json.loads(result)
    new_floating = find_nodes(tree, should_be_floating)
    for node in new_floating:
        args = ['[con_id=' + str(node['id']) + ']', 'floating', 'enable']
        swaymsg([], args)
