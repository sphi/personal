#!/usr/bin/env bash
set -euo pipefail

TERM_APP_ID="org.wezfurlong.wezterm"
TERM_COMMAND="wezterm"
KATE_APP_ID="org.kde.kate"
WHOAMI="$(whoami)"
REQUESTED_USER="${1:-$WHOAMI}"
LOGIN_AS="$REQUESTED_USER"
if test "$LOGIN_AS" == "$WHOAMI"; then
    LOGIN_AS=""
fi
CD_TO=""
SWAYMSG_TREE="$(swaymsg -t get_tree)"
WINDOW_DATA="$(jq '.. | select(.focused?) | {name, app_id}' <<<"$SWAYMSG_TREE")"
APP_ID="$(jq -r '.app_id' <<<"$WINDOW_DATA")"

get_parent_layout() {
    jq -r '.. | select(
        type == "object" and
        (.nodes | select(type == "array"))
        and (.nodes[] | select(type == "object" and .focused == true))
    ) | .layout' <<<"$SWAYMSG_TREE"
}

term_get_working_dir() {
    # Current window is a terminal
    # .name should be something like root@osprey:~/personal
    # This will parse it into an array of three elements
    WINDOW_NAME="$(jq -r .name <<<"$WINDOW_DATA")"
    IFS='@:' read -ra USER_HOST_DIR <<<"$WINDOW_NAME"
    if test "${#USER_HOST_DIR[@]}" == 3 -a "${USER_HOST_DIR[1]}" == "$(cat /proc/sys/kernel/hostname)"; then
        # Only cd to the directory of the current terminal if the requested user matches the current terminal user
        if test "${USER_HOST_DIR[0]}" == "$REQUESTED_USER"; then
            echo "${USER_HOST_DIR[2]}"
        fi
    else
        echo "Terminal name '$WINDOW_NAME' has unexpected format or wrong hostname" >&2
    fi
}

# Thanks Claude
url_encode() {
    local string="$(cat -)"
    local strlen=${#string}
    local encoded=""
    local pos c o

    for (( pos=0; pos<strlen; pos++ )); do
        c=${string:$pos:1}
        case "$c" in
            [-_a-zA-Z0-9] )
                o="${c}" ;;
            * )
                printf -v o '%%%02X' "'$c" ;;
        esac
        encoded+="${o}"
    done
    echo "${encoded}"
}

get_kate_session_dir() {
    # Current window is an instance of kate editor
    # .name should be something like tess.place: server.py  — Kate
    # we want to extract the session name (the first part)
    WINDOW_NAME="$(jq -r .name <<<"$WINDOW_DATA")"
    SESSION_NAME="$(grep -Po '.*(?=:)' <<<"$WINDOW_NAME" | url_encode)"
    REQUESTED_HOME_DIR="$(getent passwd "$REQUESTED_USER" | cut -d: -f6)"
    SESSION_FILE="$REQUESTED_HOME_DIR/.local/share/kate/sessions/$SESSION_NAME.katesession"
    SESSION_PATH="$(grep -Po '(?<=URL=file://).*' "$SESSION_FILE" | grep -v .local | head -n 1)"
    PREV_SESSION_PATH=""
    while test "$SESSION_PATH" != "$PREV_SESSION_PATH" -a ! -d "$SESSION_PATH/.git"; do
        PREV_SESSION_PATH="$SESSION_PATH"
        SESSION_PATH="$(dirname "$SESSION_PATH")"
    done
    if test "$SESSION_PATH" != "/"; then
        echo "$SESSION_PATH"
    fi
}

if test "$APP_ID" == "$TERM_APP_ID"; then
    CD_TO="$(term_get_working_dir)"
    if test ! -z "$CD_TO"; then
        if test "$(get_parent_layout)" == "tabbed"; then
            swaymsg 'split vertical; layout stacking'
        fi
    fi
elif test "$APP_ID" == "$KATE_APP_ID"; then
    CD_TO="$(get_kate_session_dir)"
fi

if test ! -z "$CD_TO" -a ! -z "$LOGIN_AS"; then
    "$TERM_COMMAND" -e login-as "$LOGIN_AS" bash -c "cd $CD_TO 2>/dev/null; exec bash"
elif test ! -z "$CD_TO"; then
    "$TERM_COMMAND" -e bash -c "cd $CD_TO && exec bash"
elif test ! -z "$LOGIN_AS"; then
    "$TERM_COMMAND" -e login-as "$LOGIN_AS"
else
    "$TERM_COMMAND"
fi
