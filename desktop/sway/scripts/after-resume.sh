#!/usr/bin/env bash

# Works around issue on ThinkPad T14S where one CPU core is periodically pegged until NM is restarted
systemctl restart NetworkManager &

python /etc/friendshaped/main.py

wait
echo "Done"
