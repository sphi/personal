#!/usr/bin/env bash
set -euo pipefail

name="$1"
cmd="$2"

PID_FILE="$XDG_RUNTIME_DIR/$name-display-pid"
if test -f "$PID_FILE" && ps -p $(cat "$PID_FILE") > /dev/null; then
  # If the file exists and contains the PID of a running process, don't start up
  exit 0
fi
# Put our PID in the file, so as long as we're running other instances don't start
echo "$$" >"$PID_FILE"

sleep_time=0.1
max_ticks=10

function watch_level {
  prev=-1
  idle_ticks=0
  while test $idle_ticks -lt $max_ticks; do
    level=$(bash -c "$cmd")
    if test $level -ne $prev; then
      prev=$level
      if test $level -ge 100; then
        echo 99.9
      else
        echo $level
      fi
      idle_ticks=0
    fi
    idle_ticks=$((idle_ticks+1))
    sleep $sleep_time
  done
}

watch_level | zenity --progress --percentage=0 --no-cancel --text='' --auto-close --title=level-display
