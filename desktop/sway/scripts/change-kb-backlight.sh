#!/usr/bin/env bash
set -euo pipefail
DIR="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")"

base="/sys/class/leds"
vendor="tpacpi::kbd_backlight"
path="$base/$vendor"

if [ ! -d $path ]; then
	echo "$path does not exist, please adjust vendor"
fi

current=$(cat $path/brightness)
max=$(cat $path/max_brightness)
change="$1"
new_p=$((current+change))
echo $new_p
if (( $new_p > $max )); then
    new_p=$max
fi
if (( $new_p < 0 )); then
    new_p=0
fi
tee $path/brightness <<< $new_p > /dev/null

"$DIR/level-display.sh" kb-backlight 'echo $(($(cat '"$path/brightness"')*100/$(cat '"$path/max_brightness"')))'
