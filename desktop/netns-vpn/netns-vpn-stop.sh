#!/usr/bin/env bash

if test $(id -u) != 0; then
  echo "Script must be run as root"
  exit 1
fi

# Stop the VPN
ip netns exec vpn wg-quick down netns-vpn
# Delete the virtual ethernet
ip link delete v-eth1
# Delete the network namespace
ip netns del vpn
