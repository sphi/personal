#!/usr/bin/env bash
set -euo pipefail

if test $(id -u) != 0; then
  echo "Script must be run as root"
  exit 1
fi

# Based on https://www.aaflalo.me/2019/08/wireguard-and-torrent-on-linux/ and other sources

modprobe wireguard || true
# Create the network namespace
ip netns add vpn
# create the virtual ethernet pair
ip link add v-eth1 type veth peer name v-peer1
# add the v-peer1 to the namespace vpn
ip link set v-peer1 netns vpn
# set IP to the interface in root namespace
ip addr add 10.200.1.1/24 dev v-eth1
# make the interface active
ip link set v-eth1 up
# add ip to the interface in the vpn namespace with a corresponding netmask.
ip netns exec vpn ip addr add 10.200.1.2/24 dev v-peer1
# make the interface active
ip -n vpn link set v-peer1 up
 # add a loopback interface in vpn namespace
ip -n vpn link set lo up
# make the traffic in vpn namespace go to root namespace through veth
ip -n vpn route add default via 10.200.1.1
# enable IP forwarding
sysctl -w net.ipv4.ip_forward=1
# Flush forward rules, policy DROP by default.
iptables -P FORWARD DROP
iptables -F FORWARD
# Flush nat rules.
iptables -t nat -F
# Get the default network interface (such as wlan0)
INTERFACE=$(ip route get 1.1.1.1 | grep -Po '(?<=(dev ))(\S+)')
# Enable masquerading of 10.200.1.0.
iptables -t nat -A POSTROUTING -s 10.200.1.0/24 -o "$INTERFACE" -j MASQUERADE
# Allow forwarding between eth0 and v-eth1.
iptables -A FORWARD -i "$INTERFACE" -o v-eth1 -j ACCEPT
iptables -A FORWARD -o "$INTERFACE" -i v-eth1 -j ACCEPT
# Allow all output traffic
iptables -P OUTPUT ACCEPT
# Run the VPN
ip netns exec vpn wg-quick up netns-vpn

# TL;DR if the line below isn't included, everything but the VPN netns loses DNS if network connectivity is lost

# wg-quick adds the netns-vpn interface to resolvconf. This is fine, except for when the system loses it's network
# connection normally it it seems in this case resolvconf (run by dhcpd) loses all DNS IPs and nothing happens, however
# if the VPN is added then resolvconf still has mullvad's DNS IP (10.64.0.1), and so it seems to use that for
# everything. This wouldn't be a problem except that for whatever reason the change does not get undone when network
# comes back up. 10.64.0.1 is only a valid DNS resolver inside the mullvad network, therefore DNS never comes back for
# the regular system (although it does come back for the VPN). We can fix this by removing the VPN interface from
# resolvconf. Since it was added initially, /etc/netns/vpn/resolv.conf should have already been set correctly, and since
# mullvad's DNS IP is static there should be no issues once it's initially set up.
resolvconf -d netns-vpn
