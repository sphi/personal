#!/usr/bin/env bash
sudo ip netns exec vpn runuser -u "$USER" -- env XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR" WAYLAND_DISPLAY="$WAYLAND_DISPLAY" login-as-inner "$@"
