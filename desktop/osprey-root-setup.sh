#!/usr/bin/env bash
set -euo pipefail

DIR="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")"

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
BLUE="\x1b[34m"
GREEN="\x1b[32m"
RED="\x1b[31m"

ARCH_PACKAGES="
networkmanager pacman-contrib sudo bash-completion base-devel acpi man-db reflector
mlocate ripgrep-all trash-cli android-tools wireless_tools usbutils
vim git python jq bc pv lsof rustup yarn python-lsp-server mypy mold meld devtools
sway xorg-xwayland wayland-utils swayidle swaybg grim zenity slurp wl-clipboard wf-recorder
xdg-desktop-portal xdg-desktop-portal-wlr xdg-desktop-portal-gtk mate-notification-daemon
wezterm lxterminal gnome-system-monitor kate keepassxc libreoffice-fresh gimp
nemo ffmpegthumbnailer filelight file-roller eog
firefox chromium noto-fonts noto-fonts-cjk
"
# NOTE: swaylock removed since we have a custom build for lock screen support
# NOTE: sccache removed since it was causing problems when building with multiple users, should be put back when this is fixed

# AUR packages
# python-sanic
# mullvad-vpn-bin
# mullvad-vpn-cli

if "$(cat /proc/sys/kernel/hostname)" != "osprey"; then
  printf "${RED}This script is specific to Osprey, it needs to be modified to run on any other system$EOL"
  exit 1
fi

if test $UID -ne 0; then
  printf "${RED}This script is meant to be run as root$EOL"
  exit 1
  # ASROOT="sudo"
  # ASCODE=""
else
  ASROOT=""
  ASCODE="machinectl shell code@.host /usr/bin/env"
fi

install_symlink() {
  if test -L "$2"; then
    if test "$(realpath "$2")" == "$1"; then
      printf "${GREEN}$2 already linked to $1$EOL"
      return
    else
      printf "${BLUE}$2 linked to $(realpath "$2"), removing...$EOL"
      rm "$2"
    fi
  elif test -e "$2"; then
    printf "${RED}$2 already exists, needs to be removed$EOL"
    exit 1
  fi
  ln -sv "$1" "$2"
}

printf "${BLUE}Installing Kate syntax highlighting files...$EOL"
KATE_SYNTAX="$HOME/.local/share/org.kde.syntax-highlighting/syntax/"
mkdir -p "$KATE_SYNTAX"
cp -v "$DIR"/kate/kate-syntax/* "$KATE_SYNTAX"
echo

printf "${BLUE}Symlinking Sway config directory...$EOL"
SWAY_DIR="$HOME/.config/sway"
rm -fv "$SWAY_DIR"
install_symlink "$DIR/sway" "$SWAY_DIR"
echo

printf "${BLUE}Symlinking wezterm config...$EOL"
mkdir -p "$HOME/.config/wezterm/"
install_symlink "$DIR/wezterm.lua" "$HOME/.config/wezterm/wezterm.lua"

if test -e "$HOME/.config/wmww_status.conf"; then
  printf "${GREEN}wmww_status config already exists$EOL"
else
  printf "${BLUE}Installing wmww_status config...$EOL"
  cp "$DIR/wmww_status.conf" "$HOME/.config/wmww_status.conf"
fi
echo

printf "${BLUE}Installing resuspend-while-lid-closed service...$EOL"
cp "$DIR/resuspend-while-lid-closed.service" /etc/systemd/system
systemctl daemon-reload
systemctl enable --now resuspend-while-lid-closed.service
echo

if systemctl status bat-logger.service >/dev/null; then
  printf "${GREEN}Battery logger service already running$EOL"
else
  printf "${BLUE}Setting up battery logger service...$EOL"
  $ASROOT "$DIR/bat-logger/install.sh"
fi
echo

if which typescript-language-server>/dev/null; then
  printf "${GREEN}typescript-language-server already installed$EOL"
else
  printf "${BLUE}Installing typescript-language-server for Kate js/ts LSP support...$EOL"
  $ASROOT npm install -g typescript-language-server typescript
fi
echo

if test ! -f "$HOME/.bashrc"; then
  printf "${BLUE}.bashrc doesn't exist, creating it...$EOL"
  touch "$HOME/.bashrc"
fi
if ! grep -P 'source .*/bashrc-shared\.sh' "$HOME/.bashrc" >/dev/null; then
  printf "${BLUE}Sourcing bashrc-shared.sh from .bashrc...$EOL"
  echo "source $DIR/bashrc-shared.sh" >>"$HOME/.bashrc"
else
  printf "${GREEN}.bashrc already sources bashrc-shared.sh$EOL"
fi
echo

printf "${BLUE}Setting up git config...$EOL"
"$DIR/git_setup.py"
echo

printf "${BLUE}Installing scripts$EOL"
$ASROOT cp -v "$DIR/login-as.sh" /usr/local/bin/login-as
$ASROOT cp -v "$DIR/login-as-inner.sh" /usr/local/bin/login-as-inner
$ASROOT cp -v "$DIR/sway/scripts/allow_all_users.sh" /usr/local/bin/xwayland-allow-all
$ASROOT cp -v "$DIR/kate-of-owner.sh" /usr/local/bin/kate-of-owner

printf "${BLUE}Setting Nemo settings$EOL"
gsettings set org.nemo.preferences show-root-warning false
dconf write /org/cinnamon/desktop/applications/terminal/exec "'lxterminal'"
$ASCODE dconf write /org/cinnamon/desktop/applications/terminal/exec "'lxterminal'"
echo

printf "${BLUE}Overriding sudo with script$EOL"
$ASROOT cp -v "$DIR/sudo-override.sh" /usr/local/bin/sudo
$ASROOT cp -v "$DIR/validate-sudo.sh" /usr/local/bin/validate-sudo
echo

printf "${BLUE}Installing wlbouncer config...$EOL"
$ASROOT cp -v "$DIR/wlbouncer.yaml" /usr/local/etc/wlbouncer.yaml
echo

printf "${BLUE}Installing .desktop file overrides...$EOL"
for DESKTOP_FILE in $(ls "$DIR/overrides"/*.desktop); do
  install_symlink "$DESKTOP_FILE" "$HOME/.local/share/applications/$(basename "$DESKTOP_FILE")"
done
echo

printf "${BLUE}Installing pam login script...$EOL"
$ASROOT cp -v "$DIR/pam_login_script.sh" /etc/pam_login_script.sh
if ! grep '/etc/pam_login_script.sh' /etc/pam.d/system-login -q; then
  printf "${RED}You need to manually add /etc/pam_login_script.sh to /etc/pam.d/system-login for it to work, see T14s Arch.md for details$EOL"
fi
echo

printf "${BLUE}Installing clean_package_cache.hook...$EOL"
$ASROOT mkdir -p /etc/pacman.d/hooks/
$ASROOT cp -v "$DIR/arch/clean_package_cache.hook" /etc/pacman.d/hooks/clean_package_cache.hook
echo

printf "${BLUE}Installing pacman packages...$EOL"
# pacman-contrib: privides paccache, needed by the pacman hook
$ASROOT pacman -S --needed $ARCH_PACKAGES
echo

# auto-key-retrieve is helpful for makepkg/the AUR, see https://bbs.archlinux.org/viewtopic.php?id=240225
if ! cat "$HOME/.gnupg/gpg.conf" | grep auto-key-retrieve; then
  printf "${BLUE}Adding auto-key-retrieve to gpg.conf...$EOL"
  echo "
# automatically fetch keys as needed from the keyserver
keyserver-options auto-key-retrieve
" >> "$HOME/.gnupg/gpg.conf"
else
  printf "${GREEN}gpg.conf already has auto-key-retrieve$EOL"
fi
echo

printf "${BLUE}Installing network namespace VPN...$EOL"
"$DIR/netns-vpn/netns-vpn-install.sh"
echo

WEB_BROWSER_DESKTOP="firefox-ff.desktop"
# xdg-settings get much faster than xdg-settings set, even if unchanged
if test "$(xdg-settings get default-web-browser)" == "$WEB_BROWSER_DESKTOP"; then
  printf "${GREEN}Default web browser already set to $WEB_BROWSER_DESKTOP$EOL"
else
  printf "${BLUE}Setting default web browser to $WEB_BROWSER_DESKTOP...$EOL"
  xdg-settings set default-web-browser "$WEB_BROWSER_DESKTOP"
fi
echo

printf "${BLUE}Setting kate-as-owner as default text editor...$EOL"
xdg-mime default kate-of-owner.desktop text/plain
update-desktop-database /root/.local/share/applications
echo

if "$DIR/get-private.sh" >/dev/null; then
  printf "${BLUE}Symlinking wifi-sync config...$EOL"
  WIFI_SYNC=$("$DIR/get-private.sh" wifi-sync)
  install_symlink "$WIFI_SYNC" "$HOME/.config/wifi-sync"
  echo

  printf "${BLUE}Syncing wifi networks...$EOL"
  wifi-sync update
  echo

  printf "${BLUE}Symlinking ssh directory...$EOL"
  SSH_SRC=$("$DIR/get-private.sh" ssh)
  SSH_DEST="$HOME/.ssh"
  install_symlink "$SSH_SRC" "$SSH_DEST"
  echo

  printf "${BLUE}Importing backed up GPG keys...$EOL"
  "$DIR/../tools/gpg-cli.sh" restore
  echo
else
  printf "${RED}Not doing setup that requires private data$EOL"
  echo
fi

printf "${GREEN}success!$EOL"
