#!/usr/bin/env bash
set -euo pipefail

if test $# -eq 0; then
    echo "no user specified"
    exit 1
elif test $# -eq 1; then
    U=$1
    ARGS=("bash")
else
    U=$1
    shift
    ARGS=("$@")
fi

if test "$U" == "$(whoami)"; then
    exec "${ARGS[@]}"
fi

# Things to test when changing this script
# - notify-send foo
# - Firefox notifications (https://www.bennish.net/web-notifications.html)
# - Firefox screen sharing (https://mozilla.github.io/webrtc-landing/gum_test.html)
# - Launch Firefox -> `laf` in new terminal -> `firefox google.com` launches in same browser
# - Audio in Firefox
# - Startup time (how long does it take for code prompt to appear after super+shift+return)

exec machinectl \
    shell \
    $U@.host \
    /usr/bin/env \
    ${WAYLAND_DISPLAY:+"WAYLAND_DISPLAY=$XDG_RUNTIME_DIR/$WAYLAND_DISPLAY"} \
    ${DISPLAY:+"DISPLAY=$DISPLAY"} \
    login-as-inner \
    "${ARGS[@]}" 2>/dev/null
