#!/usr/bin/bash
set -euo pipefail

# From https://github.com/sudo-project/sudo/blob/main/src/parse_args.c
SHORT_OPTS="+Aa:BbC:c:D:Eeg:Hh::iKklNnPp:R:r:SsT:t:U:u:Vv"
LONG_OPTS="background,preserve-env::,edit,set-home,login,remove-timestamp,list,preserve-groups,shell,other-user:,validate,askpass,auth-type:,bell,close-from:,login-class:,chdir:,group:,help,host:,reset-timestamp,no-update,non-interactive,prompt:,chroot:,role:,stdin,command-timeout:,type:,user:,version"

FLAGS=()
while test "$#" -gt 0; do
    if [[ $1 == *'='* ]]; then
        FLAGS+=("$1")
        shift
    elif getopt -o "$SHORT_OPTS" -l "$LONG_OPTS" -- "$1" | grep -P ' --$' &>/dev/null; then
        FLAGS+=("$1")
        shift
    elif getopt -o "$SHORT_OPTS" -l "$LONG_OPTS" -- "$1" "${2:-}" | grep -P ' --$' &>/dev/null; then
        FLAGS+=("$1" "$2")
        shift
        shift
    else
        break
    fi
done

exec /usr/bin/sudo "${FLAGS[@]}" /usr/local/bin/validate-sudo "$@"
