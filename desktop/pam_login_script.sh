#!/bin/sh

# This script decrypts and mounts home directories for users. Unlike pam_mount it does not unmount on logout.

# This file should be saved as `/etc/pam_login_script.sh`
# This line should be in `/etc/pam.d/system-login` below `auth include system-auth`:
# auth       optional   pam_exec.so expose_authtok stdout /etc/pam_login_script.sh

RED="\e[31m"
GREEN="\e[32m"
CYAN="\e[36m"
GRAY="\e[37m"
NORMAL="\e[0m"
PROBLEM=0

function report {
    if test $? == 0; then
        printf "${CYAN}*${NORMAL}"
    else
        printf "${RED}%s failed${NORMAL}\n" "$1"
        PROBLEM=1
    fi
}

function actionnotneeded {
    printf "${GRAY}.${NORMAL}"
}

function mountcrypthome {
    if ! findmnt /mnt/crypthome >/dev/null; then
        # This will ask for a password, but the expose_authtok argument should cause pam to pass the password on stdin
        cryptsetup luksOpen /dev/mapper/data-home crypthome
        report "cryptsetup luksOpen"
        mount --mkdir /dev/mapper/crypthome /mnt/crypthome
        report "mount crypthome"
    else
        actionnotneeded
        actionnotneeded
    fi
}

function bindmount {
    if ! findmnt $2 >/dev/null; then
        mount --mkdir --bind /mnt/crypthome/$1 $2
        report "mount /mnt/crypthome/$1 to $2"
    else
        actionnotneeded
    fi
}

if test "$PAM_USER" == "" -o "$PAM_USER" == root -o -e "/mnt/crypthome/$PAM_USER"; then
    printf "mounting directories "
else
    printf "${GRAY}not mounting anything for $PAM_USER${NORMAL}\n"
    exit 0
fi

mountcrypthome
bindmount root /root

for HOMEPATH in ls /mnt/crypthome/*; do
    UNAME=$(basename "$HOMEPATH")
    if id "$UNAME" &>/dev/null; then
        bindmount "$UNAME" "/home/$UNAME"
    fi
done

if test $PROBLEM == 0; then
    printf " ${GREEN}succeeded${NORMAL}\n"
else
    printf " ${RED}had issues${NORMAL}\n"
fi
