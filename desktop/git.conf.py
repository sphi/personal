# Git configuration data, used by personal/config/git_setup.py
{
    'user.email': 'git@phie.me',
    'user.name': 'Sophie Winter',
    'core.editor': 'vim',
    'merge.conflictstyle': 'diff3',
    'init.defaultbranch': 'main',
    'pull.rebase': 'true',
}
