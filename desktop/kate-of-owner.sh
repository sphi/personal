#!/usr/bin/env bash
set -eo pipefail
for URI in "$@"; do
    if  [[ $URI == file://* ]]; then
        FILE="$(sed 's|file://||;s|#L.*||' <<<"$URI")"
        KATE_ARG="$(sed 's/#L/:/' <<<"$URI")"
    else
        FILE="$(realpath "$URI")"
        KATE_ARG="$FILE"
    fi
    login-as "$(stat -c "%U" "$FILE" || echo root)" bash -c "source ~/.bashrc; kate $KATE_ARG"
done
