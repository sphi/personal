#!/bin/bash
set -euo pipefail
# This used to be a complicated script that didn't work, now we use reflctor
reflector --country US --latest 5 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist
