alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias diff='diff --color=auto'
alias ip='ip --color=auto'
alias wtch="watch -d -c -n 0.5"

alias kys="sudo shutdown now"
alias e="stty echo"
alias -- -e="stty -echo"

# login-as should be a symlink to/copy of login-as.sh
alias la="login-as"
alias lac="login-as code"
alias laf="login-as ff"
alias lai="login-as install"
alias lap="login-as play"
# -echoprt fixes problem on non-TTY login shells (such as those started with machinectl) where backspacing doesn't show up right on git add -p edit lines
stty -echoprt

# QT wayland disabled becuase of https://bugs.kde.org/show_bug.cgi?id=418065
# Also cursor doesn't change between pointer and carrot correctly
export QT_QPA_PLATFORM="wayland;xcb"
export QT_WAYLAND_FORCE_DPI=96
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
# QT apps get too big when an external output is connected
# This doesn't fix the problem, but it does keep me from trying to set all these again to no avail
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_ENABLE_HIGHDPI_SCALING=0
export QT_SCALE_FACTOR=1
export QT_QPA_PLATFORMTHEME=qt5ct
export QT_STYLE_OVERRIDE=kvantum
# Forces Wayland support in Firefox
export MOZ_ENABLE_WAYLAND=1
export GDK_BACKEND=wayland,x11
export MAKEFLAGS="-j$(expr $(nproc) \+ 1)"
export PATH=${PATH}:/snap/bin:/home/wmww/node_modules/.bin/
# For compiling MATE
export ACLOCAL_FLAGS="-I /usr/local/share/aclocal/"
#export SCCACHE_SERVER_UDS=$HOME/.local/sccache.sock
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
# sccache
export SCCACHE_DIR=$HOME/.sccache

# If not root
# export GIT_SSH_COMMAND="sudo ssh"

export GTK_THEME=Sweet
# code: PS_COLOR_A="8;2;22;90;173"; PS_COLOR_B="8;2;12;143;160"
# install: PS_COLOR_A="1"; PS_COLOR_B="1"
# ff: PS_COLOR_A="8;2;244;84;64"; PS_COLOR_B="8;2;197;128;5"
# comms: PS_COLOR_A="7"; PS_COLOR_B="4"
# play: PS_COLOR_A="7"; PS_COLOR_B="3"
PS_COLOR_A="${PS_COLOR_A-8;2;197;14;210}"
PS_COLOR_B="${PS_COLOR_B-8;2;110;68;222}"
# compat with older systems: PS_SEP="▛ "
PS_SEP="${PS_SEP-🬝🬃}"
SPHI_PS1="\$(CODE=\$?; test \$CODE == 0 && echo '\[\e[4${PS_COLOR_A}m\] ' || echo '\[\e[41m\]'\$CODE'\[\e[31;4${PS_COLOR_A}m\]${PS_SEP}\[\e[0;4${PS_COLOR_A}m\]')\u\[\e[3${PS_COLOR_A};4${PS_COLOR_B}m\]${PS_SEP}\[\e[0;4${PS_COLOR_B}m\]\w\[\e[0;3${PS_COLOR_B}m\]${PS_SEP}\[\e[0m\] "
# Shown on multi-line commands
SPHI_PS2="\[\e[3${PS_COLOR_B}m\]${PS_SEP}\[\e[0m\] "
