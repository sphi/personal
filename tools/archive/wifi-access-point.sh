#!/bin/bash
set -euo pipefail

# Prints info about the current WiFi connection, including looking up the access point

SSID=$(iwconfig 2>/dev/null | grep -oP '(?<=ESSID:")[^"]+')
echo "SSID: $SSID"
AP=$(iwconfig 2>/dev/null | grep -oP "(?<=Access Point: )[\w:]+")
echo "Access Point: $AP"
# Only store the end of the access point names in this script for privacy
SUFFIX=$(echo "$AP" | grep -Po '.{8}$')
printf "Device: "
case "$SUFFIX" in
"A7:B5:EC")
  echo "router, 2.4GHz"
  ;;
"A7:B5:EB")
  echo "router, 5GHz"
  ;;
"45-86-E4")
  echo "extender, 2.4GHz"
  ;;
"45:86:E5")
  echo "extender, 5GHz"
  ;;
*)
  echo "unknown"
  ;;
esac
