#!/usr/bin/env bash

cd ~/home/Documents
rm -rf analog-clock
mkdir analog-clock

for i in {0..3}; do
  for j in {0..3}; do
    cat >analog-clock/tmp.svg <<EOL
<svg xmlns="http://www.w3.org/2000/svg" viewBox="-100 0 300 600">
  <!-- Clock face -->
  <circle cx="100" cy="100" r="90" fill="white" stroke="black" stroke-width="2"/>
  
  <!-- Hour markers -->
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(0 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(30 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(60 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(90 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(120 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(150 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(180 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(210 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(240 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(270 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(300 100 100)"/>
  <line x1="100" y1="20" x2="100" y2="30" stroke="black" stroke-width="2" transform="rotate(330 100 100)"/>
  
  <!-- Hour numbers -->
  <text x="100" y="45" text-anchor="middle" font-size="12">12</text>
  <text x="155" y="105" text-anchor="middle" font-size="12">3</text>
  <text x="100" y="165" text-anchor="middle" font-size="12">6</text>
  <text x="45" y="105" text-anchor="middle" font-size="12">9</text>
  
  <!-- Hour hand (for 2:50, that's 2.83 hours, or 85 degrees) -->
  <line x1="100" y1="100" x2="100" y2="60" stroke="black" stroke-width="4" transform="rotate($((i*30)) 100 100)"/>
  
  <!-- Minute hand (for 50 minutes, that's 300 degrees) -->
  <line x1="100" y1="100" x2="100" y2="40" stroke="black" stroke-width="2" transform="rotate($((j*30)) 100 100)"/>
  
  <!-- Center dot -->
  <circle cx="100" cy="100" r="3" fill="black"/>
</svg>
EOL
    magick analog-clock/tmp.svg analog-clock/$i-$((j*5)).png
  done
done