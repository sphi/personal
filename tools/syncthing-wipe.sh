#!/usr/bin/env bash
set -euo pipefail

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
BLUE="\x1b[34m"
GREEN="\x1b[32m"
RED="\x1b[31m"

fail() {
    printf "${RED}error: $@$EOL"
    exit 1
}

if test -z "${ANDROID_ROOT:-}"; then
    echo "On Linux"
    SYNCTHING_CLI="syncthing cli"
else
    echo "On Android"
    DIR="$(realpath "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )")"
    bash "$DIR/../android/setup-syncthing-env.sh" || printf "${RED}setup-syncthing-env.sh failed :($EOL"
    SYNCTHING_ENV_PATH="$HOME/.config/syncthing.env"
    test -e "$SYNCTHING_ENV_PATH" || fail "$SYNCTHING_ENV_PATH does not exist"
    source "$SYNCTHING_ENV_PATH"
    test ! -z "$SYNCTHING_API_KEY" || fail "no API key, please edit $SYNCTHING_ENV_PATH"
    test ! -z "$SYNCTHING_BIN" || fail "no syncthing binary, please edit $SYNCTHING_ENV_PATH"
    which "$SYNCTHING_BIN" >/dev/null || fail "$SYNCTHING_BIN from $SYNCTHING_ENV_PATH can not be run"
    SYNCTHING_CLI="$SYNCTHING_BIN cli --gui-address localhost:8384 --gui-apikey $SYNCTHING_API_KEY"
    echo "Syncthing CLI command: $SYNCTHING_CLI"
fi

$SYNCTHING_CLI config folders list >/dev/null || fail "$SYNCTHING_CLI failed"
which which >/dev/null || fail "which not installed"
which jq >/dev/null || fail "jq not available"
which grep >/dev/null || fail "grep not available"
which shred >/dev/null || fail "shred not available"

wipe_folder() {
    if test "$#" -ne 1 || test -z "$1"; then
        fail "should have exactly 1 argument (the folder name to delete)"
    fi
    NEEDLE_NAME="$1"

    SEARCHED_NAMES=""
    FOLDER_ID=""
    for id in $($SYNCTHING_CLI config folders list); do
        folder_path="$($SYNCTHING_CLI config folders "$id" path get)"
        folder_name="$(basename "$folder_path")"
        SEARCHED_NAMES="$SEARCHED_NAMES $folder_name"
        if test "$folder_name" == "$NEEDLE_NAME"; then
            if test ! -z "$FOLDER_ID"; then
                fail "multiple folders called $NEEDLE_NAME"
            fi
            FOLDER_ID="$id"
        fi
    done
    echo "$FOLDER_ID" | grep -iq "-" || fail "$NEEDLE_NAME not found, folders: $SEARCHED_NAMES"

    FOLDER_PATH="$($SYNCTHING_CLI config folders "$FOLDER_ID" path get)"
    FOLDER_NAME="$(basename "$FOLDER_PATH")"
    echo "$FOLDER_NAME path: $FOLDER_PATH"
    echo "$FOLDER_PATH" | grep -iq "$NEEDLE_NAME" || fail "$FOLDER_NAME path invalid: $FOLDER_PATH"
    JSON_PATH="$(dirname "$FOLDER_PATH")/$FOLDER_NAME.json"
    echo "Dumping JSON to $JSON_PATH"
    $SYNCTHING_CLI config folders "$FOLDER_ID" dump-json | jq '.devices |= map(if .encryptionPassword == "" then . else .encryptionPassword = "ENC_PSWD" end)' >"$JSON_PATH"
    echo "Deleting $FOLDER_NAME from syncthing"
    $SYNCTHING_CLI config folders "$FOLDER_ID" delete
    TMP_PATH="$(dirname "$FOLDER_PATH")/folder-tmp"
    echo "Moving $FOLDER_PATH to $TMP_PATH"
    mv "$FOLDER_PATH" "$TMP_PATH"
    echo "Shredding files in $TMP_PATH"
    find "$TMP_PATH" -type f -exec shred -fzvxn 1 '{}' \;
    echo "Removing directory"
    rm -rf "$TMP_PATH"
}

restore_folder() {
    JSON_PATH="$1"
    if test ! -f "$JSON_PATH"; then
        fail "$JSON_PATH doesn't exist"
    fi
    RESTORE_PATH="$(cat "$JSON_PATH" | jq .path)"
    echo "Will restore to $RESTORE_PATH"
    PSWD=""
    if grep -q "ENC_PSWD" "$JSON_PATH"; then
        read -s -p "Folder encryption password: " PSWD
        if test -z "$PSWD"; then
            fail "No password specified, aborting"
        fi
    fi
    # We pass PSWD in as a jq variable here, which for some reason jq assumes is a string
    NEW_JSON=$( \
        cat "$JSON_PATH" | \
        jq --arg PSWD "$PSWD" \
            '.devices |= map(if .encryptionPassword == "ENC_PSWD" then .encryptionPassword = $PSWD else . end)' \
        || fail "issue wrangling JSON")
    echo "Restoring..."
    $SYNCTHING_CLI config folders add-json "$NEW_JSON"
    mv "$JSON_PATH" "$JSON_PATH.used"
}

show_help() {
  printf "Syncthing wipe and restore tool
COMMANDS:
  wipe <folder-basename>: wipes a syncthing folder and leaves a name.json that can be used to restore it
    replaces any encryption passwords with ENC_PSWD
  restore <name.json>: restores a folder from JSON, asking for passwords as needed
  help: show this help message
"
}

case ${1:-no_arg} in
"wipe")
  if test "$#" -ne 2; then
    fail "wipe command requires 1 additional argument"
  fi
  wipe_folder "$2"
  ;;
"restore")
  if test "$#" -ne 2; then
    fail "restore command requires 1 additional argument"
  fi
  restore_folder "$2"
  ;;
"-h" | "--help" | "help")
  show_help
  ;;
"no_arg")
  echo "no arguent given"
  show_help
  exit 1
  ;;
*)
  echo "unknown command $1"
  show_help
  exit 1
  ;;
esac
printf "${GREEN}done$EOL"
