#!/bin/python3

import os
import sys
import subprocess
import logging
import datetime
import re
import json
from typing import Tuple, List, Dict, Optional

human_date_format = '%b %-d, %Y'
output_file_prefix = 'twitter-data'
output_file_date_format = '%Y-%m-%d'
inflate_links_file = 'inflate_links.py'
default_targets = [
    os.path.expanduser('~/heavy/Resources/Backup/'),
    os.path.expanduser('~/data/heavy/Resources/Backup/'),
    # os.path.expanduser('~/Temp/twitter'),
]
rt_media_max_size = 1024 * 1024
media_max_size = 1024 * 1024 * 10
syncthing_service = ['--user', 'syncthing']

class CompressedArchive:
    name_re = re.compile(r'^twitter-(\d{4}-\d{2}-\d{2})-\w+\.zip$')

    def __init__(self, path: str, handle: str, account_id: int, date: datetime.date):
        self.path = path
        self.date = date
        self.handle = handle
        self.account_id = account_id

    def extracted_name(self) -> str:
        return self.handle + '-' + self.date.strftime(output_file_date_format)

class ExtractedArchive:
    name_re = re.compile(r'^(\w+)-(\d{4}-\d{2}-\d{2})$')

    def __init__(self, path: str, date: datetime.date) -> None:
        self.path = path
        self.date = date
        self.new_links: List[Tuple[str, str]] = []
        self.script_path = os.path.join(path, inflate_links_file)

    def inflate_links(self) -> None:
        if os.path.isfile(self.script_path):
            logging.info('inflating links in %s', self.path)
            subprocess.run([self.script_path], capture_output=True, check=True)

    def add_link(self, real_file: str, link: str):
        real_file = os.path.relpath(real_file, self.path)
        link = os.path.relpath(link, self.path)
        self.new_links.append((real_file, link))

    def export(self) -> None:
        contents = '''#! /bin/python3
import os

symlinks = {
'''
        for real_file, link in self.new_links:
            contents += "    '" + link + "': '" + real_file + "',\n"
        contents += '''
}

base = os.path.dirname(__file__)
for key, val in symlinks.items():
    src = os.path.join(base, val)
    dest = os.path.join(base, key)
    if os.path.islink(dest):
        os.remove(dest)
    if os.path.exists(dest):
        print('WARNING: ' + dest + ' exists, so did not link to ' + src)
    else:
        os.symlink(src, dest)
print('Inflated ' + str(len(symlinks)) + ' links')
'''
        with open(self.script_path, 'w') as f:
            f.write(contents)
        subprocess.run(['chmod', '+x', self.script_path], check=True)
        # Remove the files that we're turning into links
        for real_file, link in self.new_links:
            link_path = os.path.join(self.path, link)
            if os.path.exists(link_path):
                os.remove(link_path)
        logging.info('exported %d links to %s', len(self.new_links), self.script_path)

def tweet_js_path(archive_path: str) -> str:
    result = os.path.join(archive_path, 'data', 'tweet.js') # old
    if os.path.isfile(result):
        return result
    result = os.path.join(archive_path, 'data', 'tweets.js') # new
    if os.path.isfile(result):
        return result
    assert False, 'cannot find data/tweets.js file in ' + archive_path

def tweet_media_path(archive_path: str) -> str:
    result = os.path.join(archive_path, 'data', 'tweet_media') # old
    if os.path.isdir(result):
        return result
    result = os.path.join(archive_path, 'data', 'tweets_media') # new
    if os.path.isdir(result):
        return result
    assert False, 'cannot find data/tweets_media directory in ' + archive_path

def find_target(target_arg: Optional[str]) -> str:
    if target_arg is not None:
        assert os.path.isdir(target_arg), target_arg + ' is not a valid directory'
        return target_arg
    else:
        for target in default_targets:
            if os.path.isdir(target):
                return target
        assert False, 'Failed to find target, none of these exist:\n  ' + '\n  '.join(default_targets)

class Ctx:
    extracted_directory_re = re.compile(r'^' + re.escape(output_file_prefix) + r'-(\w+)-(\d+)$')

    def __init__(self) -> None:
        if '-h' in sys.argv or '--help' in sys.argv:
            print(
                'Tool that:\n' +
                ' - finds all Twitter archives in ~/Downloads and /home/ff/Downloads\n' +
                ' - extracts all new ones to the given directory, or one of the defaults\n' +
                ' - deletes unneeded large files in the archive\n' +
                ' - uses symlinks to remove duplicate data in multiple archives'
            )
            exit(0)
        target_arg = None
        if len(sys.argv) == 2:
            target_arg = sys.argv[1]
        elif len(sys.argv) > 2:
            assert False, 'Invalid number of arguments'
        self.target = find_target(target_arg)
        self.accounts: Dict[int, List[ExtractedArchive]] = {}
        self.handle_for_account_id: Dict[int, str] = {}
        self.refresh_extractions()

    def refresh_extractions(self) -> None:
        self.accounts = {}
        archive_count = 0
        logging.info('searching %s for extracted archives', self.target)
        for dir_path, match in all_matching_in_directory(self.target, Ctx.extracted_directory_re):
            default_handle = match.group(1)
            account_id = int(match.group(2))
            self.handle_for_account_id[account_id] = default_handle
            self.accounts[account_id] = []
            for archive_path, match in all_matching_in_directory(dir_path, ExtractedArchive.name_re):
                handle = match.group(1)
                date = datetime.date.fromisoformat(match.group(2))
                logging.debug('found extracted extracted archive from %s', date.strftime(human_date_format))
                self.accounts[account_id].append(ExtractedArchive(archive_path, date))
                archive_count += 1
            self.accounts[account_id].sort(key=lambda archive: archive.date)
        logging.info(
            'found %d archives of %d accounts in %s',
            archive_count,
            len(self.accounts),
            self.target
        )

    def extracted_directory_for(self, account_id: int) -> str:
        handle = self.handle_for_account_id[account_id]
        name = output_file_prefix + '-' + handle + '-' + str(account_id)
        return os.path.join(self.target, name)

    def size_of_archives(self) -> int:
        total = 0
        for account in self.accounts.values():
            for extraction in account:
                total += path_size(extraction.path)
        return total

def path_size(path: str) -> int:
    if not os.path.exists(path):
        return 0
    if not os.path.isdir(path):
        return os.lstat(path).st_size
    size = 0
    for item in os.listdir(path):
        size += path_size(os.path.join(path, item))
    return size

def format_size(size: float) -> str:
    for postfix in ['B', 'KB', 'MB', 'GB', 'TB']:
        if size < 1024:
            return str(int(size * 100) / 100) + postfix
        size /= 1024.0
    return str(size) + 'PB'

def format_reduction(initial: float, final: float) -> str:
    if initial == 0:
        initial = 0.1
    percent = int((initial - final) / initial * 1000) / 10.0
    if percent > 0:
        word = 'smaller'
    elif percent < 0:
        word = 'bigger'
    else:
        word = 'same size'
    percent = abs(percent)
    return (
        'from ' + format_size(initial) +
        ' to ' + format_size(final) +
        ' (' + str(percent) + '% ' + word + ')'
    )

def load_blessed_media_ids(archive: str) -> set[str]:
    with open(tweet_js_path(archive), 'r') as f:
        raw_contents = f.read()
    json_str = raw_contents.split('=', 1)[1]
    logging.info('parsing massive tweet.js JSON')
    tweets = json.loads(json_str)
    logging.info('finding media IDs of user\'s tweets')
    blessed: set[str] = set()
    for item in tweets:
        tweet = item['tweet']
        media_list = (
            tweet.get('extended_entities', {}).get('media', []) +
            tweet.get('entities', {}).get('media', [])
        )
        for media in media_list:
            assert isinstance(media['id'], str)
            blessed.add(media['id'])
    logging.info('found %d blessed media IDs', len(blessed))
    return blessed

def remove_non_blessed_media(media_dir: str, blessed: set[str]) -> None:
    original = path_size(media_dir)
    removed = 0
    kept = 0
    for item in os.listdir(media_dir):
        path = os.path.join(media_dir, item)
        media_id = item.split('-', 1)[0]
        if media_id not in blessed and path_size(path) > rt_media_max_size:
            os.remove(path)
            removed += 1
        else:
            kept += 1
    new_size = path_size(media_dir)
    logging.info(
        'removed %d media files and kept %d in %s. size reduced %s',
        removed,
        kept,
        os.path.basename(media_dir),
        format_reduction(original, new_size)
    )

def remove_larger_than(media_dir: str, max_size: float) -> None:
    original = path_size(media_dir)
    removed = 0
    kept = 0
    if not os.path.isdir(media_dir):
        logging.info('%s is not a directory, skipping', media_dir)
        return
    for item in os.listdir(media_dir):
        path = os.path.join(media_dir, item)
        if path_size(path) > max_size:
            os.remove(path)
            removed += 1
        else:
            kept += 1
    new_size = path_size(media_dir)
    logging.info(
        'removed %d media files larger than %s and kept the other %d in %s. size reduced %s',
        removed,
        format_size(max_size),
        kept,
        os.path.basename(media_dir),
        format_reduction(original, new_size)
    )

def remove_all_recursive(parent: str) -> None:
    assert output_file_prefix in parent
    for item in os.listdir(parent):
        path = os.path.join(parent, item)
        if os.path.isdir(path):
            remove_all_recursive(path)
        elif os.path.isfile(path):
            os.remove(path)

def trim_archive_size(archive: str) -> None:
    blessed = load_blessed_media_ids(archive)
    data_dir = os.path.join(archive, 'data')
    remove_non_blessed_media(tweet_media_path(archive), blessed)
    remove_larger_than(tweet_media_path(archive), media_max_size)
    remove_larger_than(os.path.join(data_dir, 'direct_messages_group_media'), media_max_size)
    remove_larger_than(os.path.join(data_dir, 'direct_messages_media'), media_max_size)
    remove_larger_than(os.path.join(data_dir, 'spaces_media'), 0)
    remove_all_recursive(os.path.join(archive, 'assets', 'images', 'twemoji'))
    logging.info('removed twemoji')
    try:
        os.remove(os.path.join(data_dir, 'ad-impressions.js'))
        logging.info('removed ad-impressions.js')
    except FileNotFoundError:
        pass
    try:
        os.remove(os.path.join(data_dir, 'ad-engagements.js'))
        logging.info('removed ad-engagements.js')
    except FileNotFoundError:
        pass

def all_matching_in_directory(directory: str, pattern: re.Pattern) -> List[Tuple[str, re.Match]]:
    result: List[Tuple[str, re.Match]] = []
    for item in os.listdir(directory):
        match = pattern.fullmatch(item)
        if match is not None:
            path = os.path.join(directory, item)
            result.append((path, match))
    return result

def extract_data_from_zip(path: str) -> Tuple[str, int]:
    output = subprocess.run(
        ['unzip', '-p', path, 'data/account.js'],
        capture_output=True,
        text=True,
        encoding='utf-8',
        check=True
    ).stdout
    json_str = output.split('=', 1)[1]
    data = json.loads(json_str)
    handle = data[0]['account']['username']
    account_id = int(data[0]['account']['accountId'])
    assert isinstance(handle, str) and len(handle)
    assert account_id != 0
    logging.debug('detected handle %s and account ID %d in %s', handle, account_id, path)
    return handle, account_id

def scan_archives(directory: str) -> List[CompressedArchive]:
    archives: List[CompressedArchive] = []
    logging.info('searching %s for archives', directory)
    for path, match in all_matching_in_directory(directory, CompressedArchive.name_re):
        date = datetime.date.fromisoformat(match.group(1))
        logging.debug(
            'found twitter archive %s from %s',
            os.path.basename(path),
            date.strftime(human_date_format)
        )
        handle, account_id = extract_data_from_zip(path)
        archives.append(CompressedArchive(path, handle, account_id, date))
    archives.sort(key=lambda archive: archive.date)
    logging.info('found %d archives in %s', len(archives), directory)
    return archives

def extract_archive(archive: CompressedArchive, ctx: Ctx) -> bool:
    prev_extractions = ctx.accounts.get(archive.account_id, [])
    for existing in prev_extractions:
        if existing.date == archive.date:
            logging.debug(
                'archive from %s already extracted, skipping',
                archive.date.strftime(human_date_format)
            )
            return False
    directory = ctx.extracted_directory_for(archive.account_id)
    if not os.path.exists(directory):
        logging.info('%s doesn\'t exist, creating', directory)
        os.mkdir(directory)
    dest = os.path.join(directory, archive.extracted_name())
    unzip_args = ['unzip', '-q', archive.path, '-d', dest]
    logging.info('running: `%s`', ' '.join(unzip_args))
    subprocess.run(unzip_args, check=True)
    initial_dest_size = path_size(dest)
    logging.info('archive extracted into \'%s\' (size: %s)', dest, format_size(initial_dest_size))
    trim_archive_size(dest)
    new_size = path_size(dest)
    logging.info('archive size reduced %s', format_reduction(initial_dest_size, new_size))
    if len(prev_extractions):
        old_size = path_size(prev_extractions[-1].path)
        logging.info('since previous archive, size changed %s', format_reduction(old_size, new_size))
    ctx.refresh_extractions()
    return True

def link_identical_files_in(original: str, target: str, manager: ExtractedArchive) -> int:
    if not os.path.exists(target) or os.path.islink(target):
        return 0
    # Rel links in HTML change when linked
    if original.endswith('.html') or original.endswith('.py'):
        return 0
    if os.path.isdir(original):
        count = 0
        for item in os.listdir(original):
            count += link_identical_files_in(
                os.path.join(original, item),
                os.path.join(target, item),
                manager
            )
        return count
    if subprocess.run(['cmp', '-s', original, target]).returncode == 0:
        manager.add_link(target, original)
        return 1
    else:
        # logging.info('%s has changed, leaving %s', target, original)
        return 0

def link_identical_files(original: ExtractedArchive, targets: List[ExtractedArchive]) -> int:
    count = 0
    for target in targets:
        count += link_identical_files_in(original.path, target.path, original)
    original.export()
    return count

def check_for_links(path: str) -> Tuple[int, int]:
    '''Returns a tuple of (good_count, broken_count)'''
    if os.path.islink(path):
        if os.path.exists(path):
            return 1, 0
        else:
            return 0, 1
    elif os.path.isdir(path):
        good = 0
        broken = 0
        for item in os.listdir(path):
            item_good, item_broken = check_for_links(os.path.join(path, item))
            good += item_good
            broken += item_broken
        return good, broken
    else:
        return 0, 0

def extract_all() -> None:
    ctx = Ctx()
    total_start_size = ctx.size_of_archives()
    archives: List[CompressedArchive] = []
    downloads_dir = os.path.expanduser('~/Downloads')
    if os.path.exists(downloads_dir):
        archives += scan_archives(downloads_dir)
    ff_downloads_dir = '/home/ff/Downloads'
    if os.path.exists(ff_downloads_dir):
        archives += scan_archives(ff_downloads_dir)
    for archive in reversed(archives):
        if archive.account_id not in ctx.handle_for_account_id:
            ctx.handle_for_account_id[archive.account_id] = archive.handle
    extracted_count = 0
    for archive in archives:
        if extract_archive(archive, ctx):
            extracted_count += 1
    logging.info(
        'extracted %d new archives and skipped %d existing archives',
        extracted_count,
        len(archives) - extracted_count
    )
    total_pre_link_size = ctx.size_of_archives()
    fake_link_count = 0
    for account in ctx.accounts.values():
        for extraction in account:
            extraction.inflate_links()
        for i, extraction in enumerate(account[:-1]):
            fake_link_count += link_identical_files(extraction, list(reversed(account[i+1:])))
    total_final_size = ctx.size_of_archives()
    logging.info(
        'by linking %d identical files, size changed %s',
        fake_link_count,
        format_reduction(total_pre_link_size, total_final_size)
    )
    logging.info('net size changed %s', format_reduction(total_start_size, total_final_size))
    good_links = 0
    broken_links = 0
    for account in ctx.accounts.values():
        for extraction in account:
            good, broken = check_for_links(extraction.path)
            good_links += good
            broken_links += broken
    if good or broken:
        logging.error(
            'archives have %d inflatable links, %d valid symlinks and %d broken symlinks',
            fake_link_count,
            good,
            broken)
        input('[press enter to continue]')
    else:
        logging.info('no symlinks in archives (good)')

def main() -> None:
    has_syncthing = subprocess.run(
        ['systemctl', 'is-active'] + syncthing_service,
        capture_output=True).returncode == 0
    if has_syncthing:
        logging.info('stopping syncthing')
        subprocess.run(['systemctl', 'stop'] + syncthing_service)
    else:
        logging.error('did not detect running syncthing service')
        if input('continue without stopping syncthing? [y/N] ') != 'y':
            logging.info('aborting')
            return
    try:
        extract_all()
    finally:
        if has_syncthing:
            logging.info('restarting syncthing')
            subprocess.run(['systemctl', 'start'] + syncthing_service)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
    logging.info('done')
