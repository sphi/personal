#!/bin/bash
set -euo pipefail

# Function to convert degrees to radians
deg2rad() {
    echo "$1 * 0.017453292519943295" | bc -l
}

# Function to calculate distance using Haversine formula
haversine() {
    lat1="$(deg2rad "$1")"
    lon1="$(deg2rad "$2")"
    lat2="$(deg2rad "$3")"
    lon2="$(deg2rad "$4")"

    dlat="$(echo "$lat2 - $lat1" | bc -l)"
    dlon="$(echo "$lon2 - $lon1" | bc -l)"

    a="$(echo "s($dlat/2)^2 + c($lat1) * c($lat2) * s($dlon/2)^2" | bc -l)"
    c="$(echo "2 * a(sqrt($a))" | bc -l)"

    # Earth radius in miles
    R=3958.8

    distance="$(echo "$R * $c" | bc -l)"
    printf "%.2f\n" $distance
}

lat=''
lon=''
arg2latlon() {
    if [[ "$1" == *"tess.place"* ]]; then
        echo "Fetching from tess.place..." >&2
        token="$(sed 's:.*/view/::' <<< "$1")"
        data="$(curl -s "https://tess.place/locations?t=$token")"
        lat="$(jq '.locations[0].lat' <<< "$data")"
        lon="$(jq '.locations[0].lon' <<< "$data")"
    elif grep -Pq '^[\-\d]+(\.\d*)?,[\-\d]+(\.\d*)?$' <<<"$1"; then
        lat="$(sed 's:,.*::' <<<"$1")"
        lon="$(sed 's:.*,::' <<<"$1")"
    else
        echo "Argument $1 is not a valid location" >&2
        exit 1
    fi
}

if test $# != 2; then
    echo "Should be called with two locations (each either lat,lon pair or tess.place share link)" >&2
    exit 1
fi

DATA_A="$(curl -s 'https://tess.place/locations?t=')"
DATA_B="$(curl -s 'https://tess.place/locations?t=')"

arg2latlon "$1"
lat1="$lat"
lon1="$lon"
echo "Location 1: $lat,$lon" >&2

arg2latlon "$2"
lat2="$lat"
lon2="$lon"
echo "Location 2: $lat,$lon" >&2

distance=$(haversine "$lat1" "$lon1" "$lat2" "$lon2")

echo "Distance in miles (stdout):" >&2
echo $distance
