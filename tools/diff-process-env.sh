#!/bin/bash
set -euo pipefail
# Tool that shows the diff between the environment of the given PID and the current shell
# By default uses meld, set the DIFF_TOOL environment variable to change the diff tool
DIFF_TOOL=${DIFF_TOOL:-meld}

if test $# -ne 1; then
  echo "Should have exactly one arg (the PID)"
  exit 1
fi
if ! ps "$1" &>/dev/null; then
  echo "$1 is not the PID of a running process"
  exit 1
fi
sudo cat /proc/$1/environ | tr '\0' '\n' | sort >/tmp/a.txt
cat /proc/"$$"/environ | tr '\0' '\n' | sort >/tmp/b.txt
$DIFF_TOOL /tmp/a.txt /tmp/b.txt
