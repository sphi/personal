#!/usr/bin/bash
set -euo pipefail

function yn {
    while true; do
        read -p "$1 [y/n] " result
        case "$result" in
            y | Y ) return 0;;
            n | N ) return 1;;
            * ) ;;
        esac
    done
}

function get_timestamp {
    VIDEO="$1"
    MESSAGE="$2"
    # - Open the video with the mpv video player, and the --osd-fractions option to show miliseconds
    # - Replace carrage returns with newlines so grep and tail will work
    # - Find the lines of output where the video is paused
    # - Find the last line
    # - Get the timestamp out of that line
    if ! mpv --osd-fractions --osd-msg1="$MESSAGE" "$VIDEO" 2>&1 | sed "s/$(printf '\r')/\n/g" | grep Paused | tail -n 1 | grep -Po '(?<=V: )[0-9:\.]+'; then
        read -p "mpv failed, enter manually. $MESSAGE (format: HH:MM:SS.mmm):" timestamp
        echo "$timestamp"
    fi
}

TMP_FILE=/tmp/recording.mp4
rm -rf "$TMP_FILE"
wf-recorder --codec libx264 --pixel-format yuv420p --geometry "$(slurp)" -f "$TMP_FILE"
if yn "Trim?"; then
    START="$(get_timestamp "$TMP_FILE" "Select start")"
    echo "Trim start: $START"
    END="$(get_timestamp "$TMP_FILE" "Select end (after $START)")"
    echo "Trim end: $END"
    ffmpeg -i "$TMP_FILE" -ss "$START" -to "$END" "$TMP_FILE.trimmed.mp4"
    mv "$TMP_FILE.trimmed.mp4" "$TMP_FILE"
fi
DEST_PATH="/home/ff/recording.mp4"
DEST_USER=ff
mv "$TMP_FILE" "$DEST_PATH"
chown $DEST_USER "$DEST_PATH"
echo "Your video is now at $DEST_PATH"
