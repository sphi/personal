#!/usr/bin/env bash
set -euo pipefail

NORMAL="\x1b[0m"
BLUE="\x1b[34m"
GREEN="\x1b[32m"
RED="\x1b[31m"

set_backup_path() {
  BACKUP_DIR=$("$(dirname "$(readlink -f "$0")")/../desktop/get-private.sh" gpg)
  BACKUP_FILE="$BACKUP_DIR/keys.pgp"
  printf "backup path is ${BLUE}$BACKUP_DIR${NORMAL}\n"
}

# Asks for user to paste something and leaves it in $RESULT
get_paste() {
  printf "${GREEN}Paste $1:${NORMAL}\n\n"
  # Black magic from https://stackoverflow.com/a/20913871
  IFS= read -d '' -n 1 RESULT
  while IFS= read -d '' -n 1 -t 0.2 c
  do
    RESULT+=$c
  done
}

decrypt() {
  get_paste "encrypted message"
  printf "\n\n${GREEN}Decrypted message:${NORMAL}\n"
  echo "$RESULT" | gpg --decrypt 2>&1
  echo
}

verify() {
  get_paste "signed message"
  printf "\n\n${GREEN}Result:${NORMAL}\n"
  VERIFIED=$(echo "$RESULT" | gpg --verify 2>&1 || true)
  echo "$VERIFIED"
  if echo "$VERIFIED" | grep 'Good signature from' >/dev/null; then
    SIGNER=$(echo "$VERIFIED" | grep -Po '(?<=Good signature from ").*(?=")')
    printf "\n${GREEN}Success!${NORMAL} ($SIGNER)\n"
  else
    printf "\n${RED}FAILED!${NORMAL}\n"
    exit 1
  fi
}

verify_key() {
  if ! gpg --list-key "$1" >/dev/null; then
    printf "${RED}Invalid GPG key $1${NORMAL}\n"
    exit 1
  fi
}

name_of_key() {
  gpg --list-key "$1" | grep 'uid' | grep -Po '(?<=] ).*'
}

# Argument should be --list-keys or --list-secret-keys
select_key_from_list() {
  IDS=("invalid-key")
  for ID in $(gpg $1 | grep -Po '(?<=^      )[A-Z0-9]+$'); do
    printf "${#IDS[@]}. ${BLUE}$(name_of_key "$ID")${NORMAL}\n"
    IDS+=("$ID")
  done
  printf "Select key: "
  read INDEX
  SELECTED="${IDS["$INDEX"]}"
  verify_key "$SELECTED"
}

# Result is left in $SELECTED
select_priv_key() {
  select_key_from_list --list-secret-keys
}

# Result is left in $SELECTED
select_any_key() {
  select_key_from_list --list-keys
}

encrypt() {
  printf "Select ${GREEN}origin${NORMAL}:\n"
  select_priv_key
  SENDER="$SELECTED"
  verify_key "$SENDER"
  printf "Sending from $SENDER (${BLUE}$(name_of_key "$SENDER")${NORMAL})\n\n"
  printf "Select ${GREEN}recipient${NORMAL}:\n"
  select_any_key
  RECIPIENT="$SELECTED"
  verify_key "$RECIPIENT"
  printf "Sending to $RECIPIENT (${BLUE}$(name_of_key "$RECIPIENT")${NORMAL})\n\n"
  get_paste "plaintext"
  printf "\n\n${GREEN}Encrypted message:${NORMAL}\n"
  echo "$RESULT" | gpg --encrypt --sign --armor -r "$RECIPIENT" -u "$SENDER" 2>&1
}

import_pubkey() {
  get_paste "public key"
  printf "\n\n${GREEN}Importing...${NORMAL}\n"
  echo "$RESULT" | gpg --import
  printf "\n${GREEN}Done.${NORMAL}\n"
}

generate_key() {
  printf "${GREEN}Generating key${NORMAL}\n"
  gpg --full-generate-key
  printf "\n${GREEN}Done.${NORMAL}\n"
}

backup_keys() {
  set_backup_path
  if test -e "$BACKUP_FILE"; then
    COUNTER=0
    CANDIDATE="$BACKUP_FILE"
    while test -e "$CANDIDATE"; do
      CANDIDATE="$BACKUP_FILE.$COUNTER"
      COUNTER=$((COUNTER+1))
    done
    mv "$BACKUP_FILE" "$CANDIDATE"
    printf "${GREEN}old backup moved to $CANDIDATE, with contents:${NORMAL}\n"
    gpg --import-options show-only --import "$CANDIDATE"
  fi
  gpg --output "$BACKUP_FILE" --armor --export-secret-keys --export-options export-backup
  printf "${GREEN}backed up the following keys to $BACKUP_FILE:${NORMAL}\n"
  gpg --import-options show-only --import "$BACKUP_FILE"
  printf "\n${GREEN}Done.${NORMAL}\n"
}

restore_keys() {
  set_backup_path
  gpg --import-options restore --import "$BACKUP_FILE"
}

show_keys() {
  set_backup_path
  printf "${BLUE}Keys in backup:${NORMAL}\n"
  gpg --import-options show-only --import "$BACKUP_FILE"
  printf "${BLUE}Keys on system:${NORMAL}\n"
  gpg --list-secret-keys
}

show_pubkey() {
  printf "${GREEN}Select key${NORMAL}:\n"
  select_any_key
  printf "\n${GREEN}Public key (${BLUE}$(name_of_key "$SELECTED")${GREEN}):${NORMAL}\n\n"
  gpg --export -a "$SELECTED"
}

show_help() {
  printf "${GREEN}Sophie's GPG CLI${NORMAL}
COMMANDS:
  ${BLUE}decrypt${NORMAL}: decrypt a pasted message
  ${BLUE}verify${NORMAL}: verify a pasted message
  ${BLUE}encrypt${NORMAL}: encrypt a message
  ${BLUE}import-pubkey${NORMAL}: import a pasted public key
  ${BLUE}generate${NORMAL}: generate a new key
  ${BLUE}backup${NORMAL}: backup private keys
  ${BLUE}restore${NORMAL}: restore previously backed up keys
  ${BLUE}show${NORMAL}: show all keys
  ${BLUE}show-pubkey${NORMAL}: show a specific public key
  ${BLUE}help${NORMAL}, ${BLUE}--help${NORMAL}, ${BLUE}-h${NORMAL}: show this message
"
}

if test $# -gt 1; then
  echo "too many arguments given"
  exit 1
fi

case ${1:-no_arg} in
"decrypt")
  decrypt
  ;;
"verify")
  verify
  ;;
"encrypt")
  encrypt
  ;;
"import-pubkey")
  import_pubkey
  ;;
"generate")
  generate_key
  ;;
"backup")
  backup_keys
  ;;
"restore")
  restore_keys
  ;;
"show")
  show_keys
  ;;
"show-pubkey")
  show_pubkey
  ;;
"-h" | "--help" | "help")
  show_help
  ;;
"no_arg")
  printf "${RED}no arguent given${NORMAL}\n"
  show_help
  exit 1
  ;;
*)
  printf "${RED}unknown command $1${NORMAL}\n"
  show_help
  exit 1
  ;;
esac

