#!/usr/bin/env bash
set -uoe pipefail

# This script allows running a local instance of nginx with a nginx server config without root permissions. Just give it
# the path to your .conf file and it should take care of the rest. To include a line only with this script prefix it
# with "# DEVONLY:" (no quotes). To exclude a line when running with this script suffix it with "# PRODONLY". @DEV_CWD@
# Is expaded to the directory the script was launced from and @DEV_PATH@ is the directory the .conf file lives in, both
# only work when running with this script obviously, so should generally be on DEVONLY lines.

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
BLUE="\x1b[34m"
GREEN="\x1b[1;32m"
RED="\x1b[1;31m"

CONFIG_FILE="${1:-}"
if test ! -f "$CONFIG_FILE"; then
  printf "${RED}Please pass the path to your .conf file as the first argument$EOL"
  exit 1
fi

LISTEN_PORT=8080
NGINX_DIR="/tmp/nginx-dev"
NGINX_PID_PATH="$NGINX_DIR/nginx.pid"
NGINX_CONFIG="
worker_processes 1;

events {
    worker_connections 4;
}

http {
    client_body_temp_path   /dev/null;
    proxy_temp_path         /dev/null;
    fastcgi_temp_path       /dev/null;
    uwsgi_temp_path         /dev/null;
    scgi_temp_path          /dev/null;
    access_log              /dev/stdout;
    error_log               /dev/stdout;

    include user.conf;
}"

printf "${BLUE}Installing $CONFIG_FILE into $NGINX_DIR...$EOL"
rm -rf "$NGINX_DIR"
mkdir -p "$NGINX_DIR"
cp "$CONFIG_FILE" "$NGINX_DIR/user.conf"
sed '
  s|# *DEVONLY: ||g;
  s|\([^ ].*# *PRODONLY\)|# \1|g;
  s|server *{|server { listen '"$LISTEN_PORT"';|g;
  s|@DEV_CWD@|'"$PWD"'|g;
  s|@DEV_PATH@|'"$(dirname "$(realpath "$CONFIG_FILE")")"'|g;
  ' -i "$NGINX_DIR/user.conf"
echo "$NGINX_CONFIG" > "$NGINX_DIR/nginx.conf"

function run_nginx {
  nginx -p "$NGINX_DIR" -c "$NGINX_DIR/nginx.conf" -g "pid $NGINX_PID_PATH;" $@
}

function sigint_handler {
    printf "${BLUE}SIGINT, stopping nginx$EOL"
    run_nginx -s stop
}

printf "${BLUE}Starting nginx...$EOL"
run_nginx
trap sigint_handler SIGINT
printf "${GREEN}Listening on http://localhost:$LISTEN_PORT$EOL"

set +e
while test -e "$NGINX_PID_PATH"; do
  sleep 0.1
done
set -e

printf "${BLUE}Cleaning up...$EOL"
rm -rf "$NGINX_DIR"
printf "${GREEN}Done$EOL"
