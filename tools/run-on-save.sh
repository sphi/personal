#!/usr/bin/env bash
set -euo pipefail

FREQ_MILLI=1000
WAIT_S=0.2
LAST_TIME=0

while read -r SAVED_FILE; do
    CURRENT_TIME="$(date +%s%N)"
    DELTA_TIME=$((CURRENT_TIME - LAST_TIME))
    export SAVED_FILE
    export DELTA_TIME
    if test $DELTA_TIME -gt "${FREQ_MILLI}000000"; then
        LAST_TIME=$CURRENT_TIME
        (
            sleep "${WAIT_S}s"
            bash -c "$*" || true
        )&
    fi
done < <(
    inotifywait . -r -m -q -P --excludei '/\.' -e close_write,move,create --format '%w%f'
)
