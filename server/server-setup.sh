#!/usr/bin/env bash
set -euo pipefail

# This script sets up the VPS that runs most of my web services.
# To run it on a fresh machine, make a script with these contents:

# /root/update-server-setup.sh
COMMENT='
#!/usr/bin/env bash
rm -f ~/server-setup.sh
vim ~/server-setup.sh
chmod +x ~/server-setup.sh
~/server-setup.sh
'

# Make it executable and run it. In the vim it opens, paste this script and save. It will then run the script and set
# up the server. NEVER CREATE A FILE NAMED update-server-setup.sh ON A MACHINE THAT ISN'T A SERVER

NORMAL="\x1b[0m"
EOL="${NORMAL}\n"
BLUE="\x1b[34m"
GREEN="\x1b[32m"
RED="\x1b[31m"

starscape_url='launch.openstarscape.com'

# -n: run non-interactively, if it doesn't work, explain what options are needed
# --nginx: use the nginx plugin
# -d: domain to use, additional domains can be added with additional -d arguments
# -m: email for updates
# --agree-tos: aggree to the terms of service
certbot_args=(
  '--expand' '-n' '--nginx'
  '-d' 'phie.me'
  '-d' 'tv.phie.me'
  '-d' 'spectra.phie.me'
  '-d' "$starscape_url"
  '-d' 'tess.place'
  '-d' 'emj.wtf'
  '-d' 'tess.ing'
  '-d' 'loginasroot.net'
  '-m' 'letsencrypt@phie.me'
  '--agree-tos'
)

phie_me_server_conf='
server {
    server_name phie.me;
    location / {
        return 301 https://codeberg.org/sphi$request_uri;
    }
}
'

tess_ing_server_conf='
server {
    server_name tess.ing;
    location / {
        return 301 https://twitter.com/xsphi;
    }
}
'

spectra_viewer_dir="/var/www/spectra-viewer"
spectra_viewer_server_conf='
server {
    server_name spectra.phie.me;
    location / {
      root '"$spectra_viewer_dir"'/public;
      index index.html;
  }
}
'

standard_pub_key='
# standard
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9rexiFPxDtjj/v7Tes0ofMdVZzUYwJftMSDxSauwA2RctPwaNk41ZXGyN954iAYXeyMuN70RIIAx7FTwHxeVzxhlJxV9kTH+BK5WnWO0rZaIUQNeZPUHYQsfJtVD11wyJrOfxf8eXM4cw26K/7Pwiyo7XRKeTzoTHawkgJ18aMher7qCWjc19gOjzifkgcHgGLrtt2D2MmkK+wARbVPgG7ZNJfFgssx9S/n1+6J2K+436lUE5zOd7LOBQQt/TriNu8RfSF3LbIuE45AslwP43zSPQLQD9IDYtUZnHCSJSLOWwCK4gj5t5Ha7+AAqSCuRu/xxvqhVVVYyqfL0G52yBAypDFoAIRA2m5FwF50r+xSI3vZRl1tO9jyO+ccJmfmqTfuSatc2ffjq8ruxOMFxl8ip2wXDpjf/szazTQ9Z/VLbvCxeblVCnKgC/zSmPgNZgikDNx00l3ST95Vgl8hMUuR3cIDTkwWSV8vQ+pko4ZI/J/V/v0YbGtRiarMqhXBOT3J7mj4obaAOB8xuQRAX7NfAdwBsBOOyuihmcgbavmOd4HySWjUQgID3UrAlVkZzCL2r3H2xVuUdV86H/+KR80diItsuH7kRnztdQ5KewmokFLjPHGa5+b+CmMJzd4PYKG3+y0G3mNRCAMg67oiN1iN0MZUKD1bQ8/lgTAPWt+w== wm@wmww.sh
'

portal_pub_keys='
# pigeon
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQ2GjbrhB86Iz4lh+ii2zMtzWNhRZUIYPnXDuWkMqGEdZeUAXzB506qOrj7h3d//kMoJ1K7dC1AoURhDsJuMZpJifgqmRrVaGO1Nly1/uIQ4uBCdpusMMFeBGyC+iQkfjEOVXRs+jRTTknLZOiuOCFI/zfPGiTim5tKDNMi4H7FMdZEuvBCnKVm2HX7FPiJdCfDvtvWmggOJ7svb7sOHVwIh0swJtrbq2jb7ASqILlQ9TMSuqQhRfQKnx5E5PGrcGKgOwFNdhZocaeALpq8shVenK4WVsGj4LnM0C+ieEtEmwfxkBg3dK7UwrMKeziag9a2Q6VFRmT2tUZ6CMZLsUTLFKJy7GLp+0w1MdeUv6JZwsd9Gga1LaI6wadOtfpMhDYEaFNSkBXUFbXGGSO3vmqlPjdTfoI/6BsYjC6toxkKd5+FbVLWhug/mBexge8vVexYohTm11lW+6WP0JUqRfIialPhBDTYU802WlOI2eaPadTZx28YwZFXtgnWmSdWzE= root@raspberrypi
# dove
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCg1ll/++yuD9+yQ4xxVg84cPLlXSJvt0Fpp4K0J35Tt2n9a7xzRAkNgRr6x/7G593RQ+1TOMWEd1DLLUyvMEYN94IpTyYhTNQzrWWfg95RJmFkuvZxYUtqlXBbsii42jX+W41bVKvcT76m7x0D36XvMrbeCHnDZukNvzPQ0EHGRrWCKrZJh/0MdDrnslvq25/JNMZm5eVqhKvjAWLyekL2nfTLwf1IkALlAzXPdyOoar8EV8qM11ybz7fF4rRPybUFZDLQuXU00JZ4HKAdwb587X7LxFLPofMRYs8fpkKwvnYOmSJ3ORJjzKztggYU4l8CR1Y3ViQAtHUNxrithJUIuQjgI52dFB+8ot2q85Z842O2FUdivrTT4oxKkD0W2TNqaS8kBaotrqhCnJ/VxwjJdj1umtF1eI/PYebKE4d3aId5KdqASv33z3NBTKCmeVoUuEXIWB1ZaEMQLl5htDO3HLnYVNjrv5GIgg3iAOKADBbTx74jackt3XX3GcR40hU= root@raspberrypi
'

abort() {
  printf "${RED}%s$EOL" "$1"
  exit 1
}

ensure_line() {
  # makes sure there is exactly one instance of $REGEX in the file, and it is $VALUE
  # since it checks that $VALUE matches $REGEX, should be idempotent
  FILE="$1"
  REGEX="^$2$"
  VALUE="$3"
  printf "${BLUE}ensuring %s in %s$EOL" "$VALUE" "$FILE"
  # sanity checks
  if ! grep -E "$REGEX" -q <<<"$VALUE"; then
    abort "value $VALUE doesn't match regex $REGEX"
  fi
  # escape first instance of non-standard delimiter |
  if test ! -z $(sed -E "\|$REGEX|d" <<<"$VALUE"); then
    abort "value $VALUE doesn't match regex $REGEX (sed)"
  fi
  COUNT=$(grep -E "$REGEX" "$FILE" | wc -l)
  if test "$COUNT" -gt 1; then
    abort "$FILE has $COUNT instances of regex $REGEX, should be 1 or 0"
  fi
  # do the replacement
  if test "$COUNT" -eq 0; then
    # append to end of file
    printf "\n%s\n" "$VALUE" >> "$FILE"
    printf "${GREEN}added %s to end of %s$EOL" "$VALUE" "$FILE"
  else
    CURRENT=$(grep -E "$REGEX" "$FILE")
    if test "$CURRENT" == "$VALUE"; then
      # no replacement needed
      printf "${GREEN}%s already in %s$EOL" "$CURRENT" "$FILE"
    else
      # replace
      sed -E "s|$REGEX|$VALUE|" -i.old $FILE
      printf "${GREEN}changed %s to %s in %s$EOL" "$CURRENT" "$VALUE" "$FILE"
    fi
  fi
  echo
}

check_root() {
  if test "$(whoami)" != "root"; then
    abort "not running as root"
  fi
  SETUP_SCRIPT_PATH="/root/update-server-setup.sh"
  if test ! -e "$SETUP_SCRIPT_PATH" ; then
    abort "$SETUP_SCRIPT_PATH does not exist (are you on the correct machine?)"
  fi
}

check_debian() {
  if test "$(lsb_release -is)" != "Debian"; then
    abort 'this script is current only written for Debian'
  fi
}

ensure_user() {
  if id -u "$1" &>/dev/null; then
    printf "${GREEN}user %s already exists$EOL" "$1"
  else
    printf "${BLUE}creating user %s...$EOL" "$1"
    useradd -m "$1"
    # stop annoying warning
    sudo -u "$1" git config --global pull.rebase false
    usermod --shell /bin/bash "$1"
    if test "${2:-}" == "--pswd"; then
      echo "Enter password for $1"
      passwd "$1"
    fi
    printf "${GREEN}user %s created$EOL" "$1"
  fi
  echo
}

home_of_user() {
  echo $(bash -c "cd ~$(printf %q "$1") && pwd")
}

ensure_authorized_ssh_keys() {
  USER_NAME="$1"
  AUTHORIZED_KEYS="$2"
  printf "${BLUE}setting authorized SSH keys for $USER_NAME...$EOL"
  # see https://superuser.com/a/1613980
  USER_HOME=$(home_of_user "$USER_NAME")
  OLD="$USER_HOME/.ssh/authorized_keys.old"
  NEW="$USER_HOME/.ssh/authorized_keys"
  sudo -u "$USER_NAME" mkdir -p "$USER_HOME/.ssh"
  sudo -u "$USER_NAME" touch "$OLD"
  if test -f "$NEW"; then
    sudo -u "$USER_NAME" mv "$NEW" "$OLD"
  fi
  echo "$AUTHORIZED_KEYS" | sudo -u "$USER_NAME" tee "$NEW" >/dev/null
  diff -u "$OLD" "$NEW" || true
  printf "${GREEN}authorized SSH keys set for $USER_NAME$EOL"
  echo
}

debian_update() {
  printf "${BLUE}updating...$EOL"
  apt update -y
  apt full-upgrade -y
  # psmisc needed for killall on debian
  # jq is nice to have
  apt install -y git psmisc jq
  printf "${GREEN}update complete$EOL"
  echo
}

debian_auto_updates() {
  printf "${BLUE}enabling auto updates...$EOL"
  # from https://wiki.debian.org/UnattendedUpgrades
  apt install -y unattended-upgrades
  echo unattended-upgrades unattended-upgrades/enable_auto_updates boolean true | debconf-set-selections
  dpkg-reconfigure -f noninteractive unattended-upgrades
  printf "${GREEN}auto updates enabled$EOL"
  echo
}

reduce_disk_usage() {
  # journald logs can get very long
  ensure_line '/etc/systemd/journald.conf' '.*SystemMaxUse.*=.*' 'SystemMaxUse=20M'
}

disable_ufw() {
  # ufw seems to come with debian on vultr and prevents all ports except SSH >:(
  if which ufw >/dev/null; then
    if grep -q 'inactive' <<<$(ufw status); then
      printf "${GREEN}ufw already disabled$EOL"
      echo
    else
      printf "${BLUE}disabling ufw...$EOL"
      ufw disable
      printf "${GREEN}ufw disabled$EOL"
      echo
    fi
  fi
}

setup_portal() {
  # allows devices to use this as an SSH bridge
  ensure_user portal --pswd
  # allow SSH login to portal user
  ensure_authorized_ssh_keys portal "$standard_pub_key$portal_pub_keys"
  # allow connecting directly to the target with the origin's local keys
  ensure_line '/etc/ssh/sshd_config' '.*GatewayPorts\b.*' 'GatewayPorts yes'
  sshd -t
  systemctl reload sshd
}

setup_nginx() {
  printf "${BLUE}installing nginx...$EOL"
  apt install -y nginx certbot python3-certbot-nginx
  # see https://stackoverflow.com/a/45789055
  rm -f /etc/nginx/sites-enabled/default
  printf "${BLUE}starting nginx...$EOL"
  systemctl enable --now nginx
  printf "${BLUE}clearing existing nginx configuration files...$EOL"
  rm -f /etc/nginx/conf.d/*
}

ensure_rust_for_user() {
  U="$1"
  H="$(home_of_user "$U")"
  if test ! -e "$H/.cargo/bin/rustup"; then
    printf "${BLUE}installing rustup...$EOL"
    # -s -- -y makes it run noninteractively
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sudo -u "$U" sh -s -- -y
    source "$H/.cargo/env"
  fi
  printf "${BLUE}updating rustup...$EOL"
  sudo -u "$U" "$H/.cargo/bin/rustup" update
}

ensure_web_toolchain() {
  if ! which yarn &>/dev/null; then
    printf "${BLUE}installing yarn...$EOL"
    check_debian
    # import GPG keys
    curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor >/usr/share/keyrings/yarnkey.gpg
    # add PPA
    echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" >/etc/apt/sources.list.d/yarn.list
    apt update
    apt install -y yarn
  fi
  NODE_VERSION=18
  if ! which node &>/dev/null || test "$(node --version | cut -dv -f2 | cut -d. -f1)" -lt "$NODE_VERSION"; then
    printf "${BLUE}installing node...$EOL"
    curl -fsSL https://deb.nodesource.com/setup_${NODE_VERSION}.x | sudo -E bash -
    apt install -y nodejs
  fi
}

ensure_user_service_enabled() {
  U="$1"
  SERVICE_PATH="$2"
  SERVICE_NAME="$(basename "$SERVICE_PATH")"
  printf "${BLUE}enabling+starting $SERVICE_NAME for user $U...$EOL"
  sudo -u $U mkdir -p "/home/$U/.config/systemd/user/"
  sudo -u $U cp "$SERVICE_PATH" "/home/$U/.config/systemd/user/"
  systemctl --user -M "$U@" daemon-reload
  systemctl --user -M "$U@" stop "$SERVICE_NAME" || true
  systemctl --user -M "$U@" enable --now "$SERVICE_NAME"
}

setup_starscape() {
  ensure_web_toolchain
  ensure_user starscape
  ensure_authorized_ssh_keys starscape "$standard_pub_key"
  # allow service to run when user not logged in
  loginctl enable-linger starscape
  ensure_rust_for_user starscape
  REPO_DIR="/home/starscape/starscape"
  if test ! -e "$REPO_DIR"; then
    printf "${BLUE}cloning starscape-deploy...$EOL"
    sudo -u starscape git clone --branch update-deploy https://codeberg.org/sphi/OpenStarscape.git "$REPO_DIR"
  fi
  printf "${BLUE}running starscape setup script...$EOL"
  U_ID="$(id -u starscape)"
  sudo -u starscape "PATH=$PATH:/home/starscape/.cargo/bin/" "XDG_RUNTIME_DIR=/run/user/$U_ID" "$REPO_DIR/deploy/setup.sh" init
  printf "${BLUE}installing starscape nginx configuration...$EOL"
  NGINX_CONF="/etc/nginx/conf.d/starscape.conf"
  cp "$REPO_DIR/deploy/nginx.conf" "$NGINX_CONF"
  sed "2 i \ \ \ \ server_name $starscape_url;" -i "$NGINX_CONF"
  printf "${GREEN}starscape set up$EOL"
  echo
}

setup_domain_redirects() {
  printf "${BLUE}installing phie.me nginx configuration...$EOL"
  echo "$phie_me_server_conf" >"/etc/nginx/conf.d/phie-me-redirect.conf"
  printf "${BLUE}installing tess.ing nginx configuration...$EOL"
  echo "$tess_ing_server_conf" >"/etc/nginx/conf.d/tess-ing-redirect.conf"
}

setup_tv200() {
  U=tv200
  apt install -y python3
  ensure_user $U
  ensure_authorized_ssh_keys $U "$standard_pub_key"
  # allow service to run when user not logged in
  loginctl enable-linger $U
  REPO_DIR="/home/$U/tv200"
  if test -e "$REPO_DIR"; then
    printf "${BLUE}pulling tv200...$EOL"
    sudo -u $U git -C "$REPO_DIR" pull
  else
    printf "${BLUE}cloning tv200...$EOL"
    sudo -u $U git clone https://codeberg.org/sphi/tv200.git "$REPO_DIR"
  fi
  cd "/home/$U"
  sudo -u $U mkdir -p "data"
  if test ! -e venv; then
    printf "${BLUE}settin up tv200 venv...$EOL"
    sudo -u $U python3 -m venv venv
  fi
  printf "${BLUE}installing tv200 dependencies...$EOL"
  sudo -u $U ./venv/bin/pip install -r "$REPO_DIR/backend/requirements.txt"
  ensure_user_service_enabled "$U" "$REPO_DIR/backend/tv200.service"
  printf "${BLUE}installing tv200 nginx configuration...$EOL"
  cp "$REPO_DIR/backend/nginx.conf" "/etc/nginx/conf.d/tv200.conf"
  printf "${GREEN}tv200 set up$EOL"
  echo
}

setup_tessplace() {
  U=tessplace
  apt install -y python3
  ensure_user $U
  ensure_authorized_ssh_keys $U "$standard_pub_key"
  # allow service to run when user not logged in
  loginctl enable-linger $U
  sudo -u $U mkdir -p "/home/$U/data"
  REPO_DIR="/home/$U/tessplace"
  if test -e "$REPO_DIR"; then
    printf "${BLUE}pulling tess.place...$EOL"
    sudo -u $U git -C "$REPO_DIR" pull
  else
    printf "${BLUE}cloning tess.place...$EOL"
    sudo -u $U git clone https://codeberg.org/sphi/tessplace.git "$REPO_DIR"
  fi
  cd "/home/$U"
  if test ! -e venv; then
    printf "${BLUE}setting up tess.place venv...$EOL"
    sudo -u $U python3 -m venv venv
  fi
  printf "${BLUE}installing tess.place dependencies...$EOL"
  sudo -u $U ./venv/bin/pip install -r "$REPO_DIR/backend/requirements.txt"
  ensure_user_service_enabled "$U" "$REPO_DIR/backend/tessplace.service"
  printf "${BLUE}installing tess.place nginx configuration...$EOL"
  cp "$REPO_DIR/backend/nginx.conf" "/etc/nginx/conf.d/tessplace.conf"
  printf "${GREEN}tess.place set up$EOL"
  echo
}

setup_emjwtf() {
  U=emjwtf
  apt install -y python3
  ensure_user $U
  ensure_authorized_ssh_keys $U "$standard_pub_key"
  # allow service to run when user not logged in
  loginctl enable-linger $U
  REPO_DIR="/home/$U/emjwtf"
  if test -e "$REPO_DIR"; then
    printf "${BLUE}pulling emj.wtf...$EOL"
    sudo -u $U git -C "$REPO_DIR" pull
  else
    printf "${BLUE}cloning emj.wtf...$EOL"
    sudo -u $U git clone https://codeberg.org/sphi/emjwtf.git "$REPO_DIR"
  fi
  cd "/home/$U"
  if test ! -e venv; then
    printf "${BLUE}settin up emj.wtf venv...$EOL"
    sudo -u $U python3 -m venv venv
  fi
  printf "${BLUE}installing emj.wtf dependencies...$EOL"
  sudo -u $U ./venv/bin/pip install -r "$REPO_DIR/backend/requirements.txt"
  ensure_user_service_enabled "$U" "$REPO_DIR/backend/emjwtf.service"
  printf "${BLUE}installing emj.wtf nginx configuration...$EOL"
  cp "$REPO_DIR/backend/nginx.conf" "/etc/nginx/conf.d/emjwtf.conf"
  printf "${GREEN}emj.wtf set up$EOL"
  echo
}

setup_loginasroot() {
  U=loginasroot
  apt install -y meson ninja-build
  ensure_user $U
  ensure_authorized_ssh_keys $U "$standard_pub_key"
  # allow service to run when user not logged in
  loginctl enable-linger $U
  BLOGPLAT_DIR="/home/$U/blogplat"
  if test -e "$BLOGPLAT_DIR"; then
    printf "${BLUE}pulling blogplat...$EOL"
    sudo -u $U git -C "$BLOGPLAT_DIR" pull
  else
    printf "${BLUE}cloning blogplat...$EOL"
    sudo -u $U git clone https://codeberg.org/sphi/blogplat.git "$BLOGPLAT_DIR"
  fi
  cd "$BLOGPLAT_DIR"
  printf "${BLUE}building blogplat...$EOL"
  if test ! -e build; then
    # args can be removed once server supports C23 properly
    if ! sudo -u $U meson setup build --buildtype release -D c_args="-Dbool=char -Dnullptr=NULL -std=gnu2x"; then
      printf "\n${RED}failed to setup blogplat$EOL"
      exit 1
    fi
  fi
  if ! sudo -u $U ninja -C build; then
    printf "\n${RED}failed to build blogplat$EOL"
    exit 1
  fi
  LOGINASROOT_DIR="/home/$U/loginasroot"
  if test -e "$LOGINASROOT_DIR"; then
    printf "${BLUE}pulling loginasroot...$EOL"
    sudo -u $U git -C "$LOGINASROOT_DIR" pull
  else
    printf "${BLUE}cloning loginasroot...$EOL"
    sudo -u $U git clone https://codeberg.org/sphi/loginasroot.git "$LOGINASROOT_DIR"
  fi
  cd "$LOGINASROOT_DIR"
  ensure_user_service_enabled "$U" "infra/loginasroot.service"
  printf "${BLUE}installing loginasroot nginx configuration...$EOL"
  cp "infra/nginx.conf" "/etc/nginx/conf.d/loginasroot.conf"
  printf "${GREEN}loginasroot set up$EOL"
  echo
}

set_up_spectra_viewer() {
  printf "${BLUE}setting up spectra viewer...$EOL"
  echo "$spectra_viewer_server_conf" >"/etc/nginx/conf.d/spectra-viewer.conf"
  if test -e "$spectra_viewer_dir"; then
    git -C "$spectra_viewer_dir" pull
  else
    git clone "https://codeberg.org/sphi/spectra-viewer.git" "$spectra_viewer_dir"
  fi
  printf "${GREEN}spectra viewer set up$EOL"
  echo
}

run_certbot() {
  printf "${BLUE}running certbot...$EOL"
  certbot "${certbot_args[@]}"
  printf "${GREEN}certificates installed:$EOL"
  certbot certificates
  nginx -t
  systemctl restart nginx
  printf "${GREEN}nginx set up$EOL"
  echo
}

printf "${BLUE}setting up server...$EOL"
check_root
check_debian
debian_update
debian_auto_updates
reduce_disk_usage
disable_ufw
ensure_authorized_ssh_keys root "$standard_pub_key"

setup_nginx
setup_portal
setup_starscape
setup_domain_redirects
setup_tv200
setup_tessplace
setup_emjwtf
setup_loginasroot
set_up_spectra_viewer
nginx -t
run_certbot

printf "${GREEN}done$EOL"
