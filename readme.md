# Sophie's (public) personal setup
A catch-all repo for scripts, configuration and other personal files that can be public

## Organization
There's a directory for each platform (desktop (linux), android, etc) with platform-specific scripts and config in each. The scripts in these directories may be installed by the setup script in each, but should generally not be run directly. Scripts that are meant to be run directly go in the tools/ directory.
