#!/bin/bash
set -eo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
LD_PRELOAD="$DIR/build/src/libwayland-client.so:$DIR/build/src/libwayland-server.so" WAYLAND_DEBUG=1 "$@"
