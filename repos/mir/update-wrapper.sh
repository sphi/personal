#!/bin/bash
set -eox pipefail

cd build
make wayland_generator_test_generated_files
cp tests/acceptance-tests/wayland-generator/generated.h ../tests/acceptance-tests/wayland-generator/expected.h
cp tests/acceptance-tests/wayland-generator/generated.cpp ../tests/acceptance-tests/wayland-generator/expected.cpp
