#!/bin/bash
set -eo pipefail

VALID_REPOS="release rc dev none"
SELECTED_REPO=""
for REPO in $VALID_REPOS; do
  if test "$1" == "$REPO"; then
    SELECTED_REPO="$REPO"
  fi
done
if test -z "$SELECTED_REPO"; then
  echo "Valid repo not given (valid repos are: $VALID_REPOS)"
  exit 1
fi

INSTALLED_REPOS=$(grep -Po '^deb.*mir-team/[-\w]*' /etc/apt/sources.list /etc/apt/sources.list.d/*.list | grep -Po '[^/]*$' | sort | uniq)
for REPO in $INSTALLED_REPOS; do
  echo "Has $REPO PPA, removing"
  sudo ppa-purge ppa:mir-team/$REPO
done

if test "$SELECTED_REPO" != "none"; then
  sudo add-apt-repository --update "ppa:mir-team/$SELECTED_REPO"
  sudo apt install mir-demos mir-graphics-drivers-desktop mir-test-tools
fi
