#!/bin/bash
set -eo pipefail

cd "$( dirname "${BASH_SOURCE[0]}" )"
./setup-subprojects.sh
rm -Rf build
meson build
ninja -C build
