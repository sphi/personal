#!/bin/bash
set -eo pipefail

cd "$( dirname "${BASH_SOURCE[0]}" )"
rm -Rf subprojects
mkdir subprojects
cd subprojects
git clone https://github.com/any1/neatvnc.git
git clone https://github.com/any1/aml.git
mkdir neatvnc/subprojects
cd neatvnc/subprojects
ln -s ../../aml .
echo "Subprojects set up, you may now run meson"
