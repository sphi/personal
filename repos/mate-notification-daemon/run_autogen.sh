#!/bin/bash
set -e
cd "$( dirname "${BASH_SOURCE[0]}" )"
INSTALL="$PWD/install"
export PATH="$PATH:$INSTALL/bin"
export ACLOCAL_FLAGS="-I $INSTALL/share/aclocal"
mkdir -p "$INSTALL"
if ! which mate-autogen; then
	if test ! -d "./mate-common"; then
		echo "Cloning mate-common"
		git clone git@github.com:mate-desktop/mate-common.git
	fi
	echo "Setting up mate-common"
	cd mate-common
	./autogen.sh --prefix "$INSTALL"
	make
	make install
	echo "mate-common set up"
	cd ..
fi
export CFLAGS="-Wno-deprecated-declarations $CFLAGS"
# export CFLAGS="-Wno-deprecated-declarations -Werror -g -O0 $CFLAGS"
./autogen.sh --enable-x11 --enable-wayland --prefix "$INSTALL" $@
